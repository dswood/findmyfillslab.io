import { CsvMapperHeaders, findLabelForAllowedHeaderKey } from '../records';
import { IOption } from '../types';


function createHeaderMappingOptns(headerKeys: string[]): IOption[] {
  return headerKeys.map(headerKey => ({
    value: headerKey,
    label: findLabelForAllowedHeaderKey(headerKey as CsvMapperHeaders)
  }))
}

export const CSV_HEADER_MAPPING_OPTNS = createHeaderMappingOptns(Object.keys(CsvMapperHeaders))


