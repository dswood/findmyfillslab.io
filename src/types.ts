export type AsMappedType<T> = {
  [k in keyof T & string]: T[k]
}

export type IOption = {
  value: string
  label: string
}

export type ICsv = string[][]


export enum SessionStatus {
  SIGNED_IN = 'SIGNED_IN',
  SIGNED_OUT = 'SIGNED_OUT',
  PENDING = 'PENDING',
}


//--------------
// Back-End types
//--------------
export enum BEGrainType {
  FULL = 'FULL',
  MIN = 'MIN',
  SEC = 'SEC',
}

export enum BEDataType {
  TICKS = 'TICKS',
  NBBO = 'NBBO',
  VOLUME = 'VOLUME',
  USER_FILLS = 'USER_FILLS'
}

export enum BEDataMode {
  OHLC = 'OHLC',
  TICKS = 'TICKS',
}

export type SymbolDataPayload = {
  start: number
  end: number
  mode: BEDataMode
  type: BEGrainType
  sym: string // "AAPL",
  ticks: number[][]// [[ 1561968000000, 201.48, 202, 201.48, 202, 4499 ]...]
  nbbo?: number[][]
}

export enum FEGrainType {
  MILLI = 'MILLI',
  SEC = 'SEC',
  MIN = 'MIN',
  HR = 'HR',
  DAY = 'DAY',
}

export enum BidAskType {
  BID = 'BID',
  ASK = 'ASK',
}

export enum HcDashStyleType {
  Solid = 'Solid',
  ShortDash = 'ShortDash',
  ShortDot = 'ShortDot',
  ShortDashDot = 'ShortDashDot',
  ShortDashDotDot = 'ShortDashDotDot',
  Dot = 'Dot',
  Dash = 'Dash',
  LongDash = 'LongDash',
  DashDot = 'DashDot',
  LongDashDot = 'LongDashDot',
  LongDashDotDot = 'LongDashDotDot'
}

export enum HcSeriesType {
  LINE = 'LINE',
  COLUMN = 'COLUMN',
  CANDLESTICK = 'CANDLESTICK',
  SCATTER = 'SCATTER',
  AREARANGE = 'AREARANGE', // The area range series is a carteseian series with higher and lower values for each point along an X axis, where the area between the values is shaded. Requires highcharts-more.js.
}

export enum HcAxisType {
  Y_AXIS_DEFAULT = 'Y_AXIS_DEFAULT',
  Y_AXIS_VOLUME = 'Y_AXIS_VOLUME',
}

export enum HcDataFormat {
  XY_ARRAY = 'XY_ARRAY',
  XY_OBJECT = 'XY_OBJECT',
  HIGH_LOW_OBJECT = 'HIGH_LOW_OBJECT',
}

export enum CsvTimezoneType {
  ET = 'ET',
  UTC = 'UTC',
}

export enum CsvSideType {
  BUY = 'BUY',
  SELL = 'SELL',
}

export enum CsvConfigStatusTypes {
  INCORRECT_SYMBOL_HEADER_MAPPING = 'INCORRECT_SYMBOL_HEADER_MAPPING',
  MULTIPLE_SYMBOLS_IN_CSV = 'MULTIPLE_SYMBOLS_IN_CSV',
  HAS_UNDEFINED_HEADER_MAPPINGS = 'HAS_UNDEFINED_HEADER_MAPPINGS',
  INCOMPLETE_HEADER_MAPPINGS = 'INCOMPLETE_HEADER_MAPPINGS',
  INVALID_DATE_SELECTION = 'INVALID_DATE_SELECTION',
  ALL_CONFIGS_VALID = 'ALL_CONFIGS_VALID',
}

export enum NavUpdateType {
  PAN_ON_CHART = 'PAN_ON_CHART',
  ZOOM_ON_CHART = 'ZOOM_ON_CHART',
  RIDER_STEP_ARROW = 'RIDER_STEP_ARROW',
  RIDER_BAR_DRAG = 'RIDER_BAR_DRAG',
  RIDER_HANDLE_DRAG = 'RIDER_HANDLE_DRAG',
}

export enum Dir {
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
}

export enum BEVenueTypes {
  XASE = 'XASE',
  XBOS = 'XBOS',
  XCIS = 'XCIS',
  FINR = 'FINR',
  EDGA = 'EDGA',
  EDGX = 'EDGX',
  XCHI = 'XCHI',
  XNYS = 'XNYS',
  ARCX = 'ARCX',
  XNGS = 'XNGS',
  IEXG = 'IEXG',
  XPHL = 'XPHL',
  BATY = 'BATY',
  BATS = 'BATS',
  UNKNOWN = 'N/A'
}

// TODO: define actual type params
export type HcPlotLine = any

export type HcClickEvent = {
  point?: HcClickEventPt // requires 'highcharts-custom-events'
}

export type HcClickEventPt = {
  x: number, // timestamp
  key: string, // name of point, default includes timestring
  dataGroup?: { length: number }, // dataGrouping parameters
}

export type IHcSeries = {
  name: string
  type: string
  data: number[][]
  [prop: string]: any
}

export type HcScatterData = {
  x: number
  y: number
  labelrank: number
  name: string
  isBlockTrade: boolean // not native to Highcharts
  marker?: {
    radius: number
    fillColor: string
  }
}

export type HcPieData = {
  id: string // An id for the point. This can be used after render time to get a pointer to the point object through chart.get().
  name: string
  color: string
  y: number
}

export type HcAreaRangeData = {
  x: number
  low: number
  high: number
  name: string // accessible via point.key in tooltip formatter
  // non highchart native params
  szAsk: number
  exAsk: number
  szBid: number
  exBid: number
}

export type IOhlcBucket = {
  hasBlocks: boolean,
  volume: number,
  open: number | undefined
  close: number | undefined
  high: number
  low: number
}

export type SvgRectParam = {
  x: number
  y: number
  width: number
  height: number
  fill: string
}

