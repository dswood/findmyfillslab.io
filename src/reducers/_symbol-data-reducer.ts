import { Reducer } from 'redux';
import { SET_SYMBOL_DATA } from '../actions';
import { findHcDataFormatForHcSeriesType } from '../records';
import { BEDataType, BEGrainType, HcSeriesType } from '../types';

const SEPARATOR = '-'

export function makeDataSliceKey(
  symbol: string,
  backendGrainType: BEGrainType,
  viewStart: number,
  viewEnd: number,
  hcSeriesType: HcSeriesType,
  dataType: BEDataType,
) {
  return Array.from(arguments).join(SEPARATOR)
}

export function extractParamsFromKey(sliceKey: string) {
  const [symbol, backendGrainType, viewStart, viewEnd, hcSeriesType, dataType] = sliceKey.split(SEPARATOR)
  return {
    symbol,
    backendGrainType: backendGrainType as BEGrainType,
    viewStart: Number(viewStart),
    viewEnd: Number(viewEnd),
    hcSeriesType: hcSeriesType as HcSeriesType,
    dataType: dataType as BEDataType,
  }
}

export function getHcDataFormatFromKey(sliceKey: string){
  const hcSeriesType = getHcSeriesTypeFromKey(sliceKey)
  return findHcDataFormatForHcSeriesType(hcSeriesType)
}

export function getBEGrainTypeFromKey(sliceKey: string){
  const sliceKeyParts = sliceKey.split(SEPARATOR) as (BEGrainType)[]
  return sliceKeyParts[1]
}

export function getHcSeriesTypeFromKey(sliceKey: string) {
  const sliceKeyParts = sliceKey.split(SEPARATOR) as HcSeriesType[]
  return sliceKeyParts[4]
}

export function getDataTypeFromKey(sliceKey: string){
  const sliceKeyParts = sliceKey.split(SEPARATOR) as BEDataType[]
  return sliceKeyParts[5]
}

export const COMPARE = {
  bySymbol: (sliceKeyParts: string[], symbol: string) => {
    const [ symbKey ] = sliceKeyParts
    return symbKey === symbol
  },
  byBEGrainType: (sliceKeyParts: string[], backendGrainType: string) => {
    const [ symbKey, grainType ] = sliceKeyParts
    return grainType === backendGrainType
  },
  bySymbolGranularity: (sliceKeyParts: string[], symbol: string, backendGrainType: string) => {
    const [ symbKey, grainType ] = sliceKeyParts
    return symbKey === symbol && grainType === backendGrainType
  },
  byStartEnd: (sliceKeyParts: string[], viewStart: number, viewEnd: number) => {
    const [ symbKey, grainType, start, end ] = sliceKeyParts
    return Number(start) <= viewStart && Number(end) >= viewEnd
  },
  byHcSeriesType: (sliceKeyParts: string[], seriesType: string) => {
    const [ symbKey, grainType, start, end, hcSeriesType ] = sliceKeyParts
    return seriesType === hcSeriesType
  },
}

export function findPrimarySeriesKeyUnderSymbol(dataSlices: Record<string, number[][]>, symbolKey: string, backendGrainType: string, timeMin: number, timeMax: number): string | undefined {
  const filteredKeys = selectDataSliceKeys(dataSlices, symbolKey, backendGrainType, timeMin, timeMax)
  if(filteredKeys.length > 0){ // If update contains lowest granularity data for this symbol...
    // find the primary series (candlestick) that should be used to render the preview graph
    const primarySeriesKey = filteredKeys.find(key => getDataTypeFromKey(key) === BEDataType.TICKS)
    return primarySeriesKey
  }
}

/**
 *
 * @param state
 * @param symbol
 * @param backendGrainType
 * @param viewStart finds all keys within range
 * @param viewEnd finds all keys within range
 * @param hcSeriesType
 */
export function selectDataSliceKeys<T>(state: Record<string, T>, symbol: string, backendGrainType?: string, viewStart?: number, viewEnd?: number, hcSeriesType?: string){

  return Object.keys(state).reduce((prev: string[], sliceKey)=>{
    const parts = sliceKey.split(SEPARATOR)
    if (COMPARE.bySymbol(parts, symbol)){

      if (backendGrainType !== undefined){
        if(COMPARE.byBEGrainType(parts, backendGrainType)) {

          if (viewStart !== undefined && viewEnd !== undefined){
            if(COMPARE.byStartEnd(parts, viewStart, viewEnd)){ // IMPORTANT: any key within or equal to range
              if (hcSeriesType !== undefined){
                if(COMPARE.byHcSeriesType(parts, hcSeriesType)) {
                  prev.push(sliceKey)
                }
                return prev // if
              }
              prev.push(sliceKey) // with symbol + type + min/max matches
            }
            return prev // with symbol + type + min/max matches
          }
          prev.push(sliceKey) // with symbol + type matches
        }
        return prev // with symbol + type matches
      }
      prev.push(sliceKey) // with symbol matches
      return prev // with symbol matches
    }
    return prev // with nothing
  }, [])
}

export function collectSeriesByHcSeriesTypes(state: SymbolDataState, symbSliceKeys: string[]) {
  return symbSliceKeys.reduce((prev: Record<string, number[][]>, sliceKey) => {
    const seriesType = getHcSeriesTypeFromKey(sliceKey)
    prev[seriesType] = state[sliceKey]
    return prev
  }, {})
}


export type SymbolDataAction = {
  type: string
  data: any
}

export type SymbolDataState = {
  [sliceKey: string]: number[][]
}

const DEFAULT_STATE = {}

const symbolDataReducer: Reducer<SymbolDataState> = (
  state = DEFAULT_STATE,
  action
) => {
  const { type } = (action as SymbolDataAction)
  switch (type) {

    case SET_SYMBOL_DATA: {
      const updatedStateSlices = action.data
      return Object.assign({}, state, updatedStateSlices)
    }

    default:
      return state
  }
}

export default symbolDataReducer