import { IToastProps } from "@blueprintjs/core";
import moment from 'moment-timezone';
import { Reducer } from 'redux';
import { ACTIVATE_SPINNER, ADD_TOAST, DEACTIVATE_SPINNER, POPULATE_SYMBOL_REF_DATA, SELECT_SYMBOL, SET_CLIENT_BROWSER_DIMENSIONS, SET_SELECT_FMF_CSV_DATA, SET_TIMEZONE, TOGGLE_TOOLTIPS } from '../actions';
import { TRADING_DAY_START } from '../constants';
import { IOption } from '../types';


const DEFAULT_TZ = 'GMT'
moment.tz.setDefault(DEFAULT_TZ)


export function selectSymbolOptn(symbolOptions: IOption[], symbol: string){
  return symbolOptions.find((op: IOption) => op.value === symbol)
}

export enum SpinnerHosts {
  STOCK_CHART = 'STOCK_CHART',
  FILLS_SUMMARY_TABBLE = 'FILLS_SUMMARY_TABBLE'
}

export const DEV_ONLY_SET_CSV_ENDPOINT = 'DEV_ONLY_SET_CSV_ENDPOINT'

export type IAppControlsAction = {
  type: string
  data: any
}

export type IAppControlsState = {
  symbolOptions: IOption[]
  selectedSymbolKey: string
  toasts: IToastProps[]
  isStockChartSpinnerActive: boolean
  isFillsMetricsSpinnerActive: boolean
  timeZoneString: string
  timeZoneOffsetMin: number
  devOnlyCsvEndpoint: string
  selectedFmfCsvKey: string
  browserW: number
  browserH: number
  areToolTipsActive: boolean
}

export const EMPTY_SYMBOL_KEY = '-'

const DEFAULT_STATE: IAppControlsState = {
  symbolOptions: [],
  selectedSymbolKey: EMPTY_SYMBOL_KEY,
  toasts: [],
  isStockChartSpinnerActive: false,
  isFillsMetricsSpinnerActive: false,
  timeZoneString: DEFAULT_TZ,
  timeZoneOffsetMin: 0,
  devOnlyCsvEndpoint: 'http://localhost:8080/csv/',
  selectedFmfCsvKey: '',
  browserW: 1200,
  browserH: 800,
  areToolTipsActive: true,
}

const appControlsReducer: Reducer<IAppControlsState> = (
  state = DEFAULT_STATE,
  action
) => {
  const { type, data } = (action as IAppControlsAction)
  switch (type) {

    case POPULATE_SYMBOL_REF_DATA: {
      const symbolOptions: IOption[] = action.data
      return Object.assign({}, state, {
        symbolOptions,
      })
    }

    case SELECT_SYMBOL: {
      const selectedSymbolKey: string = action.data
      if(selectedSymbolKey !== state.selectedSymbolKey){
        return Object.assign({}, state, {
          selectedSymbolKey,
        })
      }
    }

    case ADD_TOAST: {
      return Object.assign({}, state, {
        toasts: state.toasts.concat([data])
      })
    }

    case ACTIVATE_SPINNER: {
      const spinnerHost: SpinnerHosts = action.data
      switch (spinnerHost) {
        case SpinnerHosts.STOCK_CHART:
          return Object.assign({}, state, {
            isStockChartSpinnerActive: true
          })
        case SpinnerHosts.FILLS_SUMMARY_TABBLE:
          return Object.assign({}, state, {
            isFillsMetricsSpinnerActive: true
          })
      }
    }

    case DEACTIVATE_SPINNER: {
      const spinnerHost: SpinnerHosts = action.data
      switch (spinnerHost) {
        case SpinnerHosts.STOCK_CHART:
          return Object.assign({}, state, {
            isStockChartSpinnerActive: false
          })
        case SpinnerHosts.FILLS_SUMMARY_TABBLE:
          return Object.assign({}, state, {
            isFillsMetricsSpinnerActive: false
          })
      }
    }

    case SET_SELECT_FMF_CSV_DATA: {
      const selectedFmfCsvKey: string = action.data
      return Object.assign({}, state, {
        selectedFmfCsvKey,
      })
    }

    case SET_TIMEZONE: {
      const timeZoneString: string = action.data
      const newTz = moment.tz.zone(timeZoneString)
      if(newTz !== null){
        moment.tz.setDefault(timeZoneString) // change app-wide timezone
        return Object.assign({}, state, {
          timeZoneString,
          timeZoneOffsetMin: newTz.parse(TRADING_DAY_START),
        })
      }
    }

    case SET_CLIENT_BROWSER_DIMENSIONS: {
      const { browserW, browserH }: Record<string, number> = action.data
      return Object.assign({}, state, {
        browserW,
        browserH,
      })
		}
		
    case TOGGLE_TOOLTIPS: {
			const isActive: boolean = action.data
			if(isActive !== state.areToolTipsActive){
				return Object.assign({}, state, {
					areToolTipsActive: isActive,
				})
			}
    }

    case DEV_ONLY_SET_CSV_ENDPOINT: {
      return Object.assign({}, state, {
        devOnlyCsvEndpoint: action.data,
      })
    }

    default:
      return state
  }
}

export default appControlsReducer
