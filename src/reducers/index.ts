import { AnyAction, combineReducers } from 'redux';
import { RESET_APP_STATE } from '../actions';
import appControlsReducer, { IAppControlsState } from './_app-controls-reducer';
import navigatorReducer, { INavigatorState } from './_navigator-reducer';
import symbolDataReducer, { SymbolDataState } from './_symbol-data-reducer';
import userFillsReducer, { UserFillsState } from './_user-fills-reducer';

export * from './_app-controls-reducer';
export * from './_navigator-reducer';
export * from './_symbol-data-reducer';
export * from './_user-fills-reducer';


export type IApplicationState = {
  navigator: INavigatorState,
  appControls: IAppControlsState
  symbolData: SymbolDataState
  userFills: UserFillsState
}

const appReducer = combineReducers<IApplicationState>({
  navigator: navigatorReducer,
  appControls: appControlsReducer,
  symbolData: symbolDataReducer,
  userFills: userFillsReducer,
})

const rootReducer = (state: IApplicationState, action: AnyAction) => {
  if (action.type === RESET_APP_STATE) {
    return appReducer(undefined, action);
  } else {
    return appReducer(state, action);
  }
}

export default rootReducer