import { CsvParams } from '../components/CsvFileTable';


export const SAMPLE_CSV_UBNT_190613: CsvParams = {
  fileId: "sample_UBNT.csv_1563875735447",
  fileName: "sample_UBNT.csv",
  headerMappings: [{
    headerOptnMappedTo: {value: "TIME", label: "Time"},
    indexInSet: 0,
    isEnabled: true,
    originalKey: "Time",
  }, {
    headerOptnMappedTo: {value: "SYMBOL", label: "Symbol"},
    indexInSet: 1,
    isEnabled: true,
    originalKey: "Sym",
  }, {
    headerOptnMappedTo: {value: "SIZE", label: "Size"},
    indexInSet: 2,
    isEnabled: true,
    originalKey: "Shares",
  }, {
    headerOptnMappedTo: {value: "PRICE", label: "Price"},
    indexInSet: 3,
    isEnabled: true,
    originalKey: "Px",
  }, {
    headerOptnMappedTo: {value: "VENUE", label: "Venue"},
    indexInSet: 4,
    isEnabled: true,
    originalKey: "LastMarket",
  }, {
    headerOptnMappedTo: {value: "SIDE", label: "Side"},
    indexInSet: 5,
    isEnabled: true,
    originalKey: "Side",
  }],
  isCsvConfigValid: true,
  missingHeaders: [],
  headersToGenerate: [],
  normalizedCsv: undefined,
  originalCsv: [
    ["Time", "Sym", "Shares", "Px", "LastMarket", "Side"],
    ['6/13/2019 15:00:25.658', 'UBNT', '23', '132.59', 'EDGX', 'Buy'],
    ['6/13/2019 15:05:53.671', 'UBNT', '2400', '132.072', '', 'Buy'],
    ['6/13/2019 15:34:46.000', 'UBNT', '75', '132.11', '', 'Buy'],
    ['6/13/2019 16:12:35.000', 'UBNT', '100', '132.38', '', 'Buy'],
  ],
  originalHeaderKeys: ["Time", "Sym", "Shares", "Px", "LastMarket", "Side"],
  sampleDateValueInCsv: "6/13/2019 15:00:25.658",
  targetDate: "2019-06-13",
  targetSymbol: "UBNT",
  targetTimezone: "UTC",
  targetSide: "BUY",
  uniqueSymbolsInCsv: ["UBNT"],
  uploadTime: 1563875735447,
}

export const SAMPLE_CSV_BAC_FAKE_190613: CsvParams = {
  fileId: "fake_UBNT.csv_1563875735447",
  fileName: "fake_UBNT.csv",
  headerMappings: [{
    headerOptnMappedTo: {value: "TIME", label: "Time"},
    indexInSet: 0,
    isEnabled: true,
    originalKey: "Time",
  }, {
    headerOptnMappedTo: {value: "SYMBOL", label: "Symbol"},
    indexInSet: 1,
    isEnabled: true,
    originalKey: "Sym",
  }, {
    headerOptnMappedTo: {value: "SIZE", label: "Size"},
    indexInSet: 2,
    isEnabled: true,
    originalKey: "Shares",
  }, {
    headerOptnMappedTo: {value: "PRICE", label: "Price"},
    indexInSet: 3,
    isEnabled: true,
    originalKey: "Px",
  }, {
    headerOptnMappedTo: {value: "VENUE", label: "Venue"},
    indexInSet: 4,
    isEnabled: true,
    originalKey: "LastMarket",
  }, {
    headerOptnMappedTo: {value: "SIDE", label: "Side"},
    indexInSet: 5,
    isEnabled: true,
    originalKey: "Side",
  }],
  isCsvConfigValid: true,
  missingHeaders: [],
  headersToGenerate: [],
  normalizedCsv: undefined,
  originalCsv: [
    ["Time", "Sym", "Shares", "Px", "LastMarket", "Side"],
    ['6/13/2019 15:00:25.658', 'UBNT', '24', '132.59', 'EDGX', 'Buy'],
    ['6/13/2019 15:05:53.671', 'UBNT', '2400', '132.072', '', 'Buy'],
    ['6/13/2019 15:34:46.000', 'UBNT', '75', '132.11', '', 'Buy'],
    ['6/13/2019 16:12:35.000', 'UBNT', '100', '132.38', '', 'Buy'],
  ],
  originalHeaderKeys: ["Time", "Sym", "Shares", "Px", "LastMarket", "Side"],
  sampleDateValueInCsv: "6/13/2019 15:00:25.658",
  targetDate: "2019-06-13",
  targetSymbol: "UBNT",
  targetTimezone: "UTC",
  targetSide: "BUY",
  uniqueSymbolsInCsv: ["UBNT"],
  uploadTime: 1563875735447,
}

export const SAMPLE_CSV_UBNT_SINGLE_FILL: CsvParams = {
  fileId: "fake_UBNT_singe.csv_1563875735447",
  fileName: "fake_UBNT.csv",
  headerMappings: [{
    headerOptnMappedTo: {value: "TIME", label: "Time"},
    indexInSet: 0,
    isEnabled: true,
    originalKey: "Time",
  }, {
    headerOptnMappedTo: {value: "SYMBOL", label: "Symbol"},
    indexInSet: 1,
    isEnabled: true,
    originalKey: "Sym",
  }, {
    headerOptnMappedTo: {value: "SIZE", label: "Size"},
    indexInSet: 2,
    isEnabled: true,
    originalKey: "Shares",
  }, {
    headerOptnMappedTo: {value: "PRICE", label: "Price"},
    indexInSet: 3,
    isEnabled: true,
    originalKey: "Px",
  }, {
    headerOptnMappedTo: {value: "VENUE", label: "Venue"},
    indexInSet: 4,
    isEnabled: true,
    originalKey: "LastMarket",
  }, {
    headerOptnMappedTo: {value: "SIDE", label: "Side"},
    indexInSet: 5,
    isEnabled: true,
    originalKey: "Side",
  }],
  isCsvConfigValid: true,
  missingHeaders: [],
  headersToGenerate: [],
  normalizedCsv: undefined,
  originalCsv: [
    ["Time", "Sym", "Shares", "Px", "LastMarket", "Side"],
    ['6/13/2019 15:00:25.658', 'UBNT', '24', '132.59', 'EDGX', 'Buy'],
  ],
  originalHeaderKeys: ["Time", "Sym", "Shares", "Px", "LastMarket", "Side"],
  sampleDateValueInCsv: "6/13/2019 15:00:25.658",
  targetDate: "2019-06-13",
  targetSymbol: "UBNT",
  targetTimezone: "UTC",
  targetSide: "BUY",
  uniqueSymbolsInCsv: ["UBNT"],
  uploadTime: 1563875735447,
}

