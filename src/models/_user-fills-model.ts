import config from '../app.config';
import { VenueBreakdown } from '../components/UserFillsSummaryVisualization';
import { getFormatterForCsvSummaryKey } from '../formatters';
import { ADD_ON_TS_KEY, CsvMapperHeaders, findLabelForCsvFillKey, findLabelForCsvSummaryKey, MAP_CSV_FILL_KEY_TO_LABEL, MAP_CSV_SUMMARY_KEY_TO_LABEL, PayloadCsvFill, PayloadCsvFillWithTs } from '../records';
import { BEVenueTypes, CsvSideType, CsvTimezoneType, HcScatterData } from '../types';
import { roundTo } from '../util';


export function appendTimestampKey(fills: PayloadCsvFill[]): PayloadCsvFillWithTs[]{
  return fills.map(fill => Object.assign({}, fill, {
    [ADD_ON_TS_KEY]: fill.TIME // keep original unix timestamp
  }))
}

export function formatCsvFillsMetrics(fill: PayloadCsvFillWithTs){
  const csvFillsKeys = Object.keys(fill) as (keyof PayloadCsvFillWithTs)[]
  const formatted: Record<string, string> = csvFillsKeys.reduce((prev: Record<string, string|number>, fillsKey)=>{
    const value = fill[fillsKey]
    const label = findLabelForCsvFillKey(fillsKey)
    prev[label] = value
    return prev
  }, {})
  return formatted
}

export function getCsvFillsUnixTs(fill: Record<string, string>){
  return Number(fill[ADD_ON_TS_KEY])
}

const EMPTY_FILLS_TRADE = Object.keys(MAP_CSV_FILL_KEY_TO_LABEL).reduce((prev: Record<string, string>, fillsKey)=>{
  prev[findLabelForCsvFillKey(fillsKey as keyof PayloadCsvFill)] = '-'
  return prev
}, {})

export function getAdditonalSummaryMetricsFromFills(fills: PayloadCsvFill[]){
  const venueFillsParams: Record<string, { fillsCount: number, sharesCount: number }> = {}
  const totalFillsCount = fills.length

  if(totalFillsCount == 0){
    return {
      'Venue Shares Breakdown': '-',
      'Venue Fills Breakdown': '-',
      'Pct. Fills Matched': '0%',
    }
  }

  let totalSharesCount = 0
  let numMatchedFills = 0
  fills.forEach((fill)=>{
    totalSharesCount += fill.SIZE
    if(fill.matched > 0){
      numMatchedFills += 1
    }
    if(venueFillsParams[fill.VENUE] === undefined){
      venueFillsParams[fill.VENUE] = {
        fillsCount: 1,
        sharesCount: fill.SIZE,
      }
    } else {
      venueFillsParams[fill.VENUE].fillsCount += 1
      venueFillsParams[fill.VENUE].sharesCount += fill.SIZE
    }
  })

  let venueFillsBreakdown = ''
  let venueSharesBreakdown = ''
  Object.keys(venueFillsParams).forEach((venueKey)=>{
    const { fillsCount, sharesCount } = venueFillsParams[venueKey]
    venueFillsBreakdown += `${venueKey} ${roundTo(fillsCount/totalFillsCount * 100, 1)+'%'}, `
    venueSharesBreakdown += `${venueKey} ${roundTo(sharesCount/totalSharesCount * 100, 1)+'%'}, `
  })

  return {
    'Venue Fills Breakdown': venueFillsBreakdown.slice(0, -2),
    'Venue Shares Breakdown': venueSharesBreakdown.slice(0, -2),
    'Pct. Fills Matched': roundTo(numMatchedFills / fills.length * 100, 1) +'%'
  }
}


export function getVenueBreakdownFromUserFills(fills: PayloadCsvFill[] | undefined){

  const payload: {
    sharesCountTotal: number
    fillsCountTotal: number
    fillsCountMatched: number
    venueBreakdowns: Partial<Record<BEVenueTypes, VenueBreakdown>>
  } = {
    sharesCountTotal: 0,
    fillsCountTotal: 0,
    fillsCountMatched: 0,
    venueBreakdowns:{
      [BEVenueTypes.UNKNOWN]: {
        fillsCount: 0,
        sharesCount: 0,
      }
    }
  }

  if(fills === undefined){
    return payload
  }

  fills.forEach((fill)=>{
    payload.fillsCountTotal += 1
    payload.sharesCountTotal += fill.SIZE
    if(fill.matched === 0){ // UN-matched fills...
      // Apply to BEVenueTypes.UNKNOWN...
      payload.venueBreakdowns[BEVenueTypes.UNKNOWN].fillsCount += 1
      payload.venueBreakdowns[BEVenueTypes.UNKNOWN].sharesCount += fill.SIZE
    } else { // Matched fills...
      payload.fillsCountMatched += 1
      if(payload.venueBreakdowns[fill.VENUE] === undefined){
        // Register new venue...
        payload.venueBreakdowns[fill.VENUE] = {
          fillsCount: 1,
          sharesCount: fill.SIZE,
        }
      } else {
        // Apply to existing venue...
        payload.venueBreakdowns[fill.VENUE].fillsCount += 1
        payload.venueBreakdowns[fill.VENUE].sharesCount += fill.SIZE
      }
    }
  })

  return payload
}

export const EMPTY_FILLS_TRADES = [
  EMPTY_FILLS_TRADE,
  EMPTY_FILLS_TRADE,
  EMPTY_FILLS_TRADE,
  EMPTY_FILLS_TRADE,
  EMPTY_FILLS_TRADE,
]


export const MANDITORY_HEADER_MAPPINGS = [
  CsvMapperHeaders.TIME,
  CsvMapperHeaders.SYMBOL,
  CsvMapperHeaders.PRICE,
  CsvMapperHeaders.SIZE,
  // CsvMapperHeaders.SIDE,
  // CsvMapperHeaders.VENUE,
]
export const AUTO_GENERATED_HEADER_MAPPINGS = [ // mappings that, if not provided, will be auto-generated and filled in before passing off to backend...
  // CsvMapperHeaders.TIME,
  // CsvMapperHeaders.SYMBOL,
  // CsvMapperHeaders.PRICE,
  // CsvMapperHeaders.SIZE,
  CsvMapperHeaders.SIDE,
  CsvMapperHeaders.VENUE,
]

// --------------------------
// Uploaded CSV Fills Summary Metrics
// --------------------------
export type PayloadCsvSummary = {
  volume: number // 23
  count: number // 1
  avg_size: number // 23.0
  avg_price: number // 1325900
  arrival: number // 1327150
  vwap: number | null // null
  end_midpoint: number // 1327150
  reversion_px: number // 1324250
  slippage_arrival: number // -1250
  slippage_vwap: number | null
  reversion: number // 2900
  avg_markout: number // 0
}

export const EMPTY_SUMMARY_METRICS = Object.keys(MAP_CSV_SUMMARY_KEY_TO_LABEL).reduce((prev: Record<string, string>, summaryKey)=>{
  prev[findLabelForCsvSummaryKey(summaryKey as keyof PayloadCsvSummary)] = '-'
  return prev
}, {})

export function formatCsvSummaryMetrics(summaryMetrics: PayloadCsvSummary){
  const csvSummaryKeys = Object.keys(summaryMetrics) as (keyof PayloadCsvSummary)[]
  return csvSummaryKeys.reduce((prev: Record<string, string>, summaryKey)=>{
    const value = summaryMetrics[summaryKey]
    const label = findLabelForCsvSummaryKey(summaryKey)
    prev[label] = getFormatterForCsvSummaryKey(summaryKey)(value)
    return prev
  }, {})
}



function createUserFillsApiUrl(){
  return `${config.API_DOMAIN}/csv/`
}


// --------------------------
// Fetch Fills Trades & Summary Metrics
// --------------------------
export type PayloadCsv = {
  summary: PayloadCsvSummary,
  fills: PayloadCsvFill[]
  fillsAsHcSeriesData: HcScatterData[]
}

export function getCsvMetrics(
  csv: string[][],
  symbol: string,
  date: string,
  tz: CsvTimezoneType,
  side: CsvSideType,
  endpoint: string = createUserFillsApiUrl()
){
  const reqStart = Date.now()
  console.log('uploadCsvToServer', arguments)
  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  return new Promise<PayloadCsv | Error>((resolve) => {
    fetch(endpoint, {
      method: 'POST',
      credentials: 'include',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        csv,
        symbol,
        date,
        tz,
        side,
      }),
    }).then(res => {
      if(res.status === 200){
        res.json().then((payload: PayloadCsv)=>{
          const reqEnd = Date.now()
          const reqTime = reqEnd - reqStart
          console.log(payload)
          console.log(`%cFills metrics received in ${reqTime}ms`, 'background:teal;color:white')
          resolve (payload)
        })
      } else {
        res.json().then((data: { error: string }) => {
          const { error } = data
          resolve(new Error(`${res.status}: ${error}`))
        })
      }
    })
  })
}
