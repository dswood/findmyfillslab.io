const config = require(`../config/${process.env.ENV}.json`)

export default {
  "API_DOMAIN": config.API,
  "STYLESHEETS": config.STYLESHEETS || [],
  "SCRIPTS": config.SCRIPTS || [],
  "GA_ID": config.GA_ID || '',

  // ---------------
  // Multi-Beam Navigator Settings...
  "DEFAULT_BEAM_RIDER_PCT": 0.50, // beam view on switching to new beam
  "PCT_RIDER_STEPPING": 0.5, // pct of beam rider size to step left/right when beam left/right button is clicked...
  "NAV_SERIES_PREVIEW_MAX_REZ": 400, // from raw data, maximum number of points to keep for construction of data preview svg polyline graph...
  "MIN_VIEW_SIZE_ALLOWED": 500, // ms
  "DEFAULT_SYMBOL_ON_LOAD": 'BAC',
  "MIN_MOUSE_MOVE_PX_THRESHOLD": 1, // if this value is set too high, then rider will not pin to ends unless dragged really slowly

  // ---------------
  // Highchart Scatter Plot Settings...
  "TICKS_DATA_SCATTER_NODE_RADIUS_MIN": 4, // radius of smallest plot point
  "TICKS_DATA_SCATTER_NODE_RADIUS_MAX": 70, // radius of largest plot point
  "TICKS_SIZE_HIGH": 20000,
  "TICKS_SIZE_LOW": 100,
  "HC_TURBO_THRESHOLD": 50000,
  "HC_VOL_BAR_HEIGHT_PCT": 0.15, // height of the volume chart as a percentage of overall chart height
}
