import moment from 'moment';
import { HcScatterData, IOhlcBucket } from '../types';


export function handleFetchErrors(res: Response) {
  if (res.ok){ // The ok read-only property of the Response interface contains a Boolean stating whether the response was successful (status in the range 200-299) or not.
    console.log('response.ok --> ',res.url)
  } else {
    throw Error(res.statusText)
  }
  return res;
}

export function compose<R>(fn1: (a: R) => R, ...fns: Array<(a: R) => R>){
  return fns.reduceRight((prevFn, nextFn) => value => prevFn(nextFn(value)), fn1);
}

export function pick<T, K extends keyof T>(obj: T, keys: K[]): Pick<T, K>{
  const picked: any = {};
  keys.forEach(key => {
    picked[key] = obj[key];
  })
  return picked;
}

export function rgbToRgba(rgb: string, alpha: number){
  const valsBtwBrackets = rgb.match(/\((.*?)\)/)
  return valsBtwBrackets !== null ?
    `rgba(${valsBtwBrackets[1]}, ${alpha})`
      : rgb
}

export function withQuotes(str: string){
  return '"' + str + '"'
}
export function withCommas(val: any): string {
  if(val === undefined || val === null || isNaN(val)) return String(val)
  const [ decimalLeft, decimalRight ] = val.toString().split('.')
  const decimalLeftWithCommas = decimalLeft.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  return decimalRight !== undefined ?
    decimalLeftWithCommas + '.' + decimalRight
      : decimalLeftWithCommas
}

export function formatUnix(time: number | Date, tz?: string){
  // multiplied by 1000 so that the argument is in milliseconds, not seconds.
  let date: number | Date
  if(time instanceof Date) date = time;
  else date = new Date(time);

  let hr, min , sec
  if(tz === 'ET'){
    hr = String(date.getHours())
    min = '0' + date.getMinutes()
    sec = '0' + date.getSeconds()
  } else { // output UTC time by default...
    hr = String(date.getUTCHours())
    min = '0' + date.getUTCMinutes()
    sec = '0' + date.getUTCSeconds()
  }
  // return display time in 10:30:23 format
  return `${hr}:${min.substr(-2)}:${sec.substr(-2)}`
}



export function timezonedDateToUtc(timezonedDate: Date, tz: string){
  // return moment.tz(timezonedDate.valueOf(), tz).valueOf()
  return moment.utc(timezonedDate.valueOf()).valueOf()
}


export function getOption<T>(ops: Record<string, T>[], key: string, val: T){
  return ops.find(op => op[key] === val)
}

export function getLast<T>(items: T[]): T{
  return items[items.length - 1]
}

export function areStrArraysIdentical(...strArrays: string[][]){
  let thisString = '',
      prevString = ''
  for (let i = 0; i < strArrays.length; i++) {
    thisString = strArrays[i].sort().join('--')
    if(i === 0){
      prevString = thisString
    } else {
      if(thisString !== prevString) return false;
      prevString = thisString
    }
  }
  return true
}

/**
 * Scale an array of numbers to be within a range of min, max
 * @param rangeVals Array of numbers to scale
 * @param newMin Minimum of new bounding range
 * @param newMax Maximum of new bounding range
 */
export function scaleNumbersToRange(rangeVals: number[], newMin: number, newMax: number) {
  const oldMin = rangeVals[0]
  const oldMax = rangeVals[rangeVals.length - 1]
  const riderSize = oldMax - oldMin
  const deltaMin = newMin - oldMin
  const deltaMax = newMax - oldMax
  // Slide old range values to start at new min
  const oldRangeValsAtNewMin = rangeVals.map((val) => {
    return val + deltaMin
  })
  const oldMaxAfterSliding = newMin + riderSize
  const oldMaxSlidingAmt = oldMaxAfterSliding - newMax // amount that old max needs to slide to become new max, this amount represents the maximum sliding range values will need to do to become new range
  return oldRangeValsAtNewMin.map((val, i) => {
    const deltaFromMin = val - newMin
    const slidingAmt = deltaFromMin / riderSize * oldMaxSlidingAmt
    return val - slidingAmt
  })
}


export function capBtw(num: number, min: number, max: number) {
  return Math.min(Math.max(num, min), max)
}


export function getItemsBeforeAfter<T>(array: T[], refIndex: number) {
  return {
    itemsBefore: array.slice(0, refIndex),
    itemsAfter: array.slice(refIndex),
  }
}


export function toRgbaString(val: number, opacity: number = 1.0){
  return `rgb(${val}, ${val}, ${val}, ${opacity})`
}

export function toPctString(val: number, decimalPlace: number = 0) {
  return (val * 100).toFixed(decimalPlace) + '%'
}


export function subDivideRange(rangeMin:number, rangeMax:number, numSteps: number){
  const rangeSize = rangeMax - rangeMin
  const stepSize = rangeSize / numSteps
  const rangeOfVals: number[] = []
  // Create new array of [numSteps] values...
  for (let i = 0; i < numSteps; i++) {
    rangeOfVals.push(rangeMin + stepSize * i)
  }
  return rangeOfVals
}

export function subDivideRangeToBuckets(rangeMin:number, rangeMax:number, numSteps: number){
  const rangeSize = rangeMax - rangeMin
  const stepSize = rangeSize / numSteps
  const rangeOfBuckets: [number, number][] = []
  // Create new array of [numSteps] values...
  for (let i = 0; i < numSteps - 1; i++) {
    rangeOfBuckets.push([
      rangeMin + stepSize * i,
      rangeMin + stepSize * (i + 1),
    ])
  }
  return rangeOfBuckets
}

export function deRezData(seriesData: number[][], numPts: number){
  if(!Array.isArray(seriesData) || !(seriesData.length >= 1)) return []

  const xMin = seriesData[0][0]
  const xMax = seriesData[seriesData.length - 1][0]
  const newXVals = subDivideRange(xMin, xMax, numPts)

  let lastIndex = 0
  let xy: number[]
  let prevXy: number[]
  let xDeltaBtwPts: number
  let xTravelFromPrev: number
  let yDeltaBtwPts: number
  // Per eacg x value in the derezed array, look through the original array and either find the exact matching y value or interporate a new y value from points directly behind and ahead...
  return newXVals.map((x)=>{
    for (let i = lastIndex; i < seriesData.length; i++) {
      xy = seriesData[i]
      if (x > xy[0]) continue // not yet reached, keep going
      if (x === xy[0]){ // found it
        lastIndex = Number(i) // save index so next iteration could skip ahead
        return xy
      }
      if (x < xy[0]){ // gone too far
        prevXy = seriesData[i - 1]
        // interpolate y value
        xDeltaBtwPts = xy[0] - prevXy[0]
        xTravelFromPrev = x - prevXy[0]
        yDeltaBtwPts = xy[1] - prevXy[1]
        lastIndex = Number(i) // save index so next iteration could skip ahead
        return [x, prevXy[1] + xTravelFromPrev / xDeltaBtwPts * yDeltaBtwPts]
      }
    }
  })
}



export function bucketScatterPlotData(
  scatterSeries: HcScatterData[],
  numBuckets: number
): {
  buckets: IOhlcBucket[]
  globalPxHigh: number
  globalPxLow: number
  globalVolHigh: number
  globalVolLow: number
} | undefined {
  if(!Array.isArray(scatterSeries) || !(scatterSeries.length >= 1)) return undefined

  const xMin = scatterSeries[0].x
  const xMax = scatterSeries[scatterSeries.length - 1].x
  const newXBuckets = subDivideRangeToBuckets(xMin, xMax, numBuckets)

  let lastIndex = 0,
      ohlcBucket: IOhlcBucket,
      globalPxHigh = 0,
      globalPxLow = Infinity,
      globalVolHigh = 0,
      globalVolLow = Infinity

  const buckets = newXBuckets.map(([ bucketMin, bucketMax ])=>{
    ohlcBucket = {
      volume: 0,
      open: undefined,
      close: undefined,
      high:0,
      low: Infinity,
      hasBlocks: false
    }
    for (let i = lastIndex; i < scatterSeries.length; i++) {
      const { x, y, labelrank, isBlockTrade }  = scatterSeries[i]
      if(x >= bucketMin && x <= bucketMax ){
        ohlcBucket.volume += labelrank
        if(ohlcBucket.hasBlocks === false && isBlockTrade){
          ohlcBucket.hasBlocks = true // set bucket to contain block if ANY of its children is a block trade
        }
        if(ohlcBucket.open === undefined){ // Initial entry into bucket boundary...
          ohlcBucket.open = y
        }
        ohlcBucket.high = y > ohlcBucket.high ? y : ohlcBucket.high
        ohlcBucket.low = y < ohlcBucket.low ? y : ohlcBucket.low
        ohlcBucket.close = y // continuously update
      } else if (x > bucketMax){ // Gone past bucket boundary...
        lastIndex = i
        break
      }
      // Challenge for global bucket high-low...
      globalPxHigh = ohlcBucket.high > globalPxHigh ? ohlcBucket.high : globalPxHigh
      globalPxLow = ohlcBucket.low < globalPxLow ? ohlcBucket.low : globalPxLow
      globalVolHigh = ohlcBucket.volume > globalVolHigh ? ohlcBucket.volume : globalVolHigh
      globalVolLow = ohlcBucket.volume < globalVolLow ? ohlcBucket.volume : globalVolLow
    }
    return ohlcBucket
  })

  return {
    buckets,
    globalPxHigh,
    globalPxLow,
    globalVolHigh,
    globalVolLow,
  }
}


export function debounce(callback: any, wait: number, immediate?: boolean) {
  // 'private' variable for instance
  // The returned function will be able to reference this due to closure.
  // Each call to the returned function will share this common timer.
  let timeout: NodeJS.Timeout | null
  // Calling debounce returns a new anonymous function
  return function(){
    const args = Array.from(arguments)
    // Should the function be called now? If immediate is true
    //   and not already in a timeout then the answer is: Yes
    var callNow = immediate && !timeout
    // This is the basic debounce behaviour where you can call this
    //   function several times, but it will only execute once
    //   [before or after imposing a delay].
    //   Each time the returned function is called, the timer starts over.
    if (timeout !== null){
      clearTimeout(timeout)
    }
    // Set the new timeout
    timeout = setTimeout(() => {
      // Inside the timeout function, clear the timeout variable
      // which will let the next execution run when in 'immediate' mode
      timeout = null
      // Check if the function already ran with the immediate flag
      if (!immediate) {
        // Call the original function with apply
        // apply lets you define the 'this' object as well as the arguments
        //    (both captured before setTimeout)
        callback.apply(null, args);
      }
    }, wait);
    // Immediate mode and no wait timer? Execute the function..
    if (callNow) callback.apply(null, args);
  }
}



export function coinFlip<T>(heads:T, tails:T): T{
  const coin = [heads, tails]
  return coin[Math.round(Math.random())]
}


export function getTimeSeriesRange(series: number[][]){
  if(series.length >= 1){
    return [ series[0][0], series[series.length-1][0]]
  } else {
    return [0, 0]
  }
}

export function roundTo(val: number, decimalPlace: number){
  const powOf10 = 10 ** decimalPlace
  return Math.floor(val * powOf10) / powOf10
}

export function padDigit(val: number) {
  return val < 10 ? '0' + val : String(val)
}



export function getMMDDYYYY(date: Date){
  const yyyy = date.getUTCFullYear()
  const mm = padDigit(date.getUTCMonth() + 1)
  const dd = padDigit(date.getUTCDate())
  return yyyy + '-' + mm + '-' + dd
}

export function getYesterday(){
  const [mm, dd, yyyy] = getMMDDYYYY(new Date()).split('-')
  return createUTCDate(yyyy, mm, +dd - 1)
}

export function createUTCDate(
  year: number | string,
  mon: number | string,
  day: number | string,
  hr: number | string = 0,
  min: number | string = 0,
  sec: number | string = 0,
  milli: number | string = 0,
){
  return new Date(Date.UTC(+year, +mon - 1, +day, +hr, +min, +sec, +milli))
}


export function getTradingDayStart(yyyy: string|number, mm: string|number, dd: string|number){
  return createUTCDate(yyyy, mm, dd, 13, 30).valueOf()
}


export function isSameArray<T>(arr1: T[], arr2: T[]): boolean {
  return arr1.every(item => arr2.indexOf(item) !== -1)
}


// https://medium.com/@martin_hotell/react-typescript-and-defaultprops-dilemma-ca7f81c661c7
export function createPropsGetter<DP extends object>(defaultProps: DP){
  return <P extends Partial<DP>>(props: P) => {
    type PropsExcludingDefaults = Pick<P, Exclude<keyof P, keyof DP>>
    type RecomposeProps = DP & PropsExcludingDefaults
    return (props as any) as RecomposeProps
  }
}


export function getRanks<T>(items: T[], compare:(a: T, b:T)=>number): number[]{
  const mapItemToOriginalIndex = new Map()
  const itemsCopy = items.slice()
  itemsCopy.forEach((item, originalIndex) => {
    mapItemToOriginalIndex.set(item, originalIndex)
  })
  const sortedItems = itemsCopy.sort((a, b) => compare(a, b))
  const sortedIndexes = sortedItems.map((item) => mapItemToOriginalIndex.get(item))
  return sortedIndexes
}

export function actualizeStrVal(strVal: string){
  const val = Number(strVal)
  return !isNaN(val) ? val :
    strVal === "undefined" ? undefined
      : strVal === "null" ? null
        : strVal === "true" ? true
          : strVal === "false" ? false
            : strVal
}

export function frameRangeAroundCenter(newRangeCenter: number, rangeSize: number){
  return {
    start: newRangeCenter - rangeSize / 2,
    end: newRangeCenter + rangeSize/ 2,
  }
}


export function strEnum<T extends string>(o: Array<T>): {[K in T]: K} {
  return o.reduce((res, key) => {
    res[key] = key;
    return res;
  }, Object.create(null));
}


export function withSelectTextOnInputFocus(el:HTMLInputElement | HTMLTextAreaElement){
  el.addEventListener('focus', ()=> el.select())
}


export function isNumber(val: any): boolean{
  return (
    val !== null
    && typeof val !== 'boolean'
    && !isNaN(Number(val))
  )
}


export function getDateFromUtcUnix(unix: number): Date{
  const date = new Date()
  date.setTime(unix)
  return date
}


export function updateArrayItem<T>(arrItems: T[] , itemIndex: number, itemPropsToUpdate: Partial<T>){
  const itemToUpdate = arrItems[itemIndex]
  arrItems[itemIndex] = Object.assign({}, itemToUpdate, itemPropsToUpdate)
  return arrItems
}


export function find<T>(arrItems: T[], item: T){
  return arrItems.find(o => o === item)
}

export function getIndexesBtw(start: number, end: number){
  const indexes: number[] = []
  for (let i = start; i <= end; i++) {
    indexes.push(i)
  }
  return indexes
}

export function deDupArray<T>(array: T[]){
  return Array.from(new Set(array))
}

