import React from 'react';
import { IOption } from '../../types';
import { CsvParams } from '../CsvFileTable';
import HeaderMapperRow from './HeaderMapperRow';
import { getDisabledHeaderMappings, getEnabledHeaderMappings } from './util';
export * from './HeaderMapperRow';
export * from './util';



export type IHeaderMapping = {
  originalKey: string
  indexInSet: number
  headerOptnMappedTo: IOption
  isEnabled: boolean
}


interface Props {
  csvInfo: CsvParams | undefined
  onHeaderMappingChange: (updatedMapping: IHeaderMapping) =>  void
}

interface State {
}

class HeaderMapper extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
    }
  }

  renderRows(headerMappings: IHeaderMapping[]){
    const { onHeaderMappingChange } = this.props

    return headerMappings.map((headerSchema) => (
      <HeaderMapperRow
        key={headerSchema.originalKey}
        headerSchema={headerSchema}
        onMappingChange={onHeaderMappingChange}
      />
    ))
  }

  render(){
    const { csvInfo } = this.props
    if (csvInfo === undefined) return null

    const { headerMappings } = csvInfo
    const enabledHeaders = getEnabledHeaderMappings(headerMappings)
    const disabledHeaders = getDisabledHeaderMappings(headerMappings)

    return (
      <div id="h-mapper-container">

        <div className="h-mapper-row-container">
          <span className="h-mapper-row-field">
            &nbsp;
          </span>
          <span className="h-mapper-row-field">
            CSV Header Value
          </span>
          <span className="h-mapper-row-field">
            Mapped To
          </span>
        </div>

        <div className="h-mapper-row-divider"/>

        {this.renderRows(enabledHeaders)}

        <div className="h-mapper-row-divider"/>

        {this.renderRows(disabledHeaders)}

      </div>
    )
  }
}

export default HeaderMapper