import { Checkbox, Intent, Tag } from "@blueprintjs/core";
import React from 'react';
import Select, { OnChangeHandler } from 'react-select';
import { CSV_HEADER_MAPPING_OPTNS } from '../../schemas/header-mapping';
import { IOption } from '../../types';
import { withQuotes } from '../../util';
import { IHeaderMapping } from './types';
import { isMappingDefined } from './util';




type Props = {
  headerSchema: IHeaderMapping
  onMappingChange: (updatedSchema: IHeaderMapping) => void
}
type State = {
  selectedHeaderOptn: IOption
}



class HeaderMapperRow extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      selectedHeaderOptn: CSV_HEADER_MAPPING_OPTNS[0],
    }
  }

  handleNormalizeHeader: OnChangeHandler = (selectedOptn) => {
    const { headerSchema } = this.props
    console.log('selectedHeaderOptn', selectedOptn)
    if (selectedOptn !== null){
      this.props.onMappingChange(
        Object.assign({}, headerSchema, {
          headerOptnMappedTo: selectedOptn
        }
      ))
    }
  }

  handleToggleEnableHeader = () => {
    const { headerSchema } = this.props
    this.props.onMappingChange(
      Object.assign({}, headerSchema, {
        isEnabled: !headerSchema.isEnabled
      }
    ))
  }

  render(){
    const { headerSchema } = this.props
    const { originalKey, isEnabled, headerOptnMappedTo } = headerSchema

    const tagIntent = isMappingDefined(headerOptnMappedTo) ? Intent.PRIMARY : Intent.DANGER

    return (
      <div className="h-mapper-row-container">
        <Checkbox
          className="h-mapper-row-checkbox"
          checked={isEnabled}
          large={true}
          onChange={this.handleToggleEnableHeader}
          />
        <Tag
          className="h-mapper-row-field"
          intent={!isEnabled ? Intent.NONE : tagIntent}
          minimal={!isEnabled}
          large={false}
          fill={false}
          >
          {withQuotes(originalKey)}
        </Tag>
        <Select
          className="h-mapper-row-field"
          options={CSV_HEADER_MAPPING_OPTNS}
          disabled={!isEnabled}
          onChange={this.handleNormalizeHeader}
          autoBlur={true}
          searchable={false}
          clearable={false}
          value={headerOptnMappedTo}
          />
      </div>
    )
  }
}

export default HeaderMapperRow