import { Menu, MenuItem } from "@blueprintjs/core";
import { Cell, Column, ColumnHeaderCell, CopyCellsMenuItem, IMenuContext, ITableProps, Table as Bp3Table, Utils } from '@blueprintjs/table';
import React from 'react';

type IFormatter = (val: string | number) => string
type IComparator = (a: any, b: any) => number

export type ICellLookup = (rowIndex: number, columnIndex: number) => string | number;
export type ISortCallback = (columnIndex: number, comparator: (a: any, b: any) => number) => void

export type InternalColumnSchema = {
  getColumn(getCellData: ICellLookup, sortColumn: ISortCallback): JSX.Element;
}

abstract class AbstractSortableColumn implements InternalColumnSchema {
  constructor(protected name: string, protected index: number, protected alignment: string, protected formatter?: IFormatter) {
  }
  protected abstract renderMenu(sortColumn: ISortCallback): JSX.Element

  public getColumn(getCellData: ICellLookup, sortColumn: ISortCallback) {
    const cellStyle = { textAlign: this.alignment } as React.CSSProperties
    const cellRenderer = (rowIndex: number, columnIndex: number) => {
      let cellData = getCellData(rowIndex, columnIndex)
      if(cellData !== '' && this.formatter !== undefined){
        cellData = this.formatter(cellData)
      }
      return (
        <Cell style={cellStyle}>
          {cellData}
        </Cell>
      )
    }

    const menuRenderer = this.renderMenu.bind(this, sortColumn);
    const columnHeaderCellRenderer = () => <ColumnHeaderCell name={this.name} menuRenderer={menuRenderer} style={cellStyle}/>;
    return (
      <Column
        cellRenderer={cellRenderer}
        columnHeaderCellRenderer={columnHeaderCellRenderer}
        key={this.index}
        name={this.name}
        />
    )
  }
}

class NonSortableColumn implements InternalColumnSchema {
  constructor(protected name: string, protected index: number, protected alignment: string, protected formatter?: IFormatter) {
  }

  public getColumn(getCellData: ICellLookup) {
    const cellStyle = { textAlign: this.alignment } as React.CSSProperties
    const cellRenderer = (rowIndex: number, columnIndex: number) => {
      let cellData = getCellData(rowIndex, columnIndex)
      if(cellData !== '' && this.formatter !== undefined){
        cellData = this.formatter(cellData)
      }
      return (
        <Cell style={cellStyle}>
          {cellData}
        </Cell>
      )
    }

    const columnHeaderCellRenderer = () => <ColumnHeaderCell name={this.name} style={cellStyle}/>;
    return (
      <Column
        cellRenderer={cellRenderer}
        columnHeaderCellRenderer={columnHeaderCellRenderer}
        key={this.index}
        name={this.name}
        />
    )
  }
}


class TextSortableColumn extends AbstractSortableColumn {
  private compare(a: any, b: any) {
    return a.toString().localeCompare(b)
  }
  protected renderMenu(sortColumn: ISortCallback) {
    const sortAsc = () => sortColumn(this.index, (a, b) => this.compare(a, b));
    const sortDesc = () => sortColumn(this.index, (a, b) => this.compare(b, a));
    return (
      <Menu>
        <MenuItem icon="sort-asc" onClick={sortAsc} text="Sort A -> Z" />
        <MenuItem icon="sort-desc" onClick={sortDesc} text="Sort Z -> A" />
      </Menu>
    );
  }
}

class NumericSortableColumn extends AbstractSortableColumn {
  private compare(a: any, b: any) {
    return a - b
  }
  protected renderMenu(sortColumn: ISortCallback) {
    const sortAsc = () => sortColumn(this.index, (a, b) => this.compare(a, b));
    const sortDesc = () => sortColumn(this.index, (a, b) => this.compare(b, a));
    return (
      <Menu>
        <MenuItem icon="sort-asc" onClick={sortAsc} text="Sort Asc" />
        <MenuItem icon="sort-desc" onClick={sortDesc} text="Sort Desc" />
      </Menu>
    );
  }
}


export enum TableDataType {
  TEXT = 'TEXT',
  NUMBER = 'NUMBER',
  NO_TYPE = 'NO_TYPE',
}
export enum TableAlignType {
  LEFT = 'LEFT',
  MIDDLE = 'MIDDLE',
  RIGHT = 'RIGHT',
}

export type UserColSchema = {
  label: string
  key: string
  valType: TableDataType
  alignment: TableAlignType
  formatter?: IFormatter
}


function extractCellDataToArray(columns: UserColSchema[], rowsData: Record<string, any>){
  return columns.map((colTemp: UserColSchema) => {
    return rowsData[colTemp.key]
  })
}

function normalizeData<T>(columns: UserColSchema[], rowsData: T[]): any[]{
  return rowsData.map((data: T) => {
    return extractCellDataToArray(columns, data)
  })
}

type Props = {
  rowsData: { [columnKey: string]: any }[]
  columnSchemas: UserColSchema[]
  bp3Options?: ITableProps
}
type State = {
  tableData: any[][]
  sortedIndexMap: number[]
  sortedColumnIndex: number
  sortingComparator: IComparator
  bp3ColumnSchemas: InternalColumnSchema[]
}


function enhanceColumnsWithSorting(columnSchemas: UserColSchema[]): InternalColumnSchema[]{
  return columnSchemas.map((colTemp: UserColSchema, columnIndex: number)=>{
    const { label, valType, alignment, formatter } = colTemp
    switch (valType) {
      case TableDataType.TEXT:
        return new TextSortableColumn(label, columnIndex, alignment, formatter)
      case TableDataType.NUMBER:
        return new NumericSortableColumn(label, columnIndex, alignment, formatter)
      case TableDataType.NO_TYPE:
      default:
        return new NonSortableColumn(label, columnIndex, alignment, formatter)
    }
  })
}

class TableInteractive extends React.Component<Props, State> {

  constructor(props: Props){
    super(props)
    const { rowsData } = props
    this.sortColumn = this.sortColumn.bind(this)
    this.renderBodyContextMenu = this.renderBodyContextMenu.bind(this)

    this.state = {
      tableData: normalizeData(props.columnSchemas, rowsData),
      sortedColumnIndex: 0,
      sortingComparator: () => 0,
      sortedIndexMap: [],
      bp3ColumnSchemas: enhanceColumnsWithSorting(props.columnSchemas),
    }
  }

  componentWillReceiveProps(nextProps: Props){
    const { rowsData, columnSchemas } = nextProps
    if (
      rowsData !== this.props.rowsData
      || columnSchemas !== this.props.columnSchemas
    ){
      const tableData = normalizeData(nextProps.columnSchemas, rowsData)
      this.sortColumn(this.state.sortedColumnIndex, this.state.sortingComparator, tableData) // call setState internally and updates sorted index and comparator
      this.setState({
        tableData,
        bp3ColumnSchemas: enhanceColumnsWithSorting(columnSchemas),
      })
    }
  }

  getCellData = (rowIndex: number, columnIndex: number): string | number => {
    const { sortedIndexMap, tableData } = this.state
    const sortedRowIndex = sortedIndexMap[rowIndex];
    if (sortedRowIndex !== undefined) {
      rowIndex = sortedRowIndex;
    }

    if(tableData[rowIndex] !== undefined){
      return this.state.tableData[rowIndex][columnIndex];
    } else {
      return ''
    }
  }

  renderBodyContextMenu(context: IMenuContext){
    return (
      <Menu>
        <CopyCellsMenuItem context={context} getCellData={this.getCellData} text="Copy" />
      </Menu>
    );
  }

  sortColumn(columnIndex: number, comparator: IComparator, stateData?: any[]){
    const tableData = stateData || this.state.tableData

    const sortedIndexMap = Utils.times(tableData.length, (i: number) => i);
    sortedIndexMap.sort((a: number, b: number) => {
      return comparator(tableData[a][columnIndex], tableData[b][columnIndex]);
    });
    this.setState({
      sortedColumnIndex: columnIndex,
      sortingComparator: comparator,
      sortedIndexMap,
    });
  }

  render(){
    const { bp3Options } = this.props
    const { bp3ColumnSchemas } = this.state
    const columnComponents = bp3ColumnSchemas.map(col => col.getColumn(this.getCellData, this.sortColumn))

    return (
      <Bp3Table
        bodyContextMenuRenderer={this.renderBodyContextMenu}
        {...(bp3Options || {})}
        >
        {columnComponents}
      </Bp3Table>
    )
  }
}

export default TableInteractive