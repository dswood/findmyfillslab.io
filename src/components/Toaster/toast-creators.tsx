import { Intent, IToastProps } from '@blueprintjs/core';
import { BEGrainType } from '../../types';
import { CsvParams } from '../CsvFileTable';

export function createReqToast(backendGrainType: BEGrainType, symbol: string, min: number, max: number): IToastProps {
  return {
    timeout: 1000,
    intent: Intent.PRIMARY,
    message: (
      <>
        <div>
          <h4>POST:</h4>
          <p>symbol: <strong>{symbol}</strong></p>
          <p>granularity: <strong>{backendGrainType}</strong></p>
          <p>view start: <strong>{min}</strong></p>
          <p>view end: <strong>{max}</strong></p>
        </div>
      </>
    ),
  }
}

export function createUploadCsvToast(csvParams: CsvParams): IToastProps {
  const { normalizedCsv, targetSymbol, targetDate, targetTimezone, targetSide } = csvParams
  return {
    timeout: 4000,
    intent: Intent.PRIMARY,
    message: (
      <>
        <div>
          <h4>sending CSV via POST request:</h4>
          <p><strong>csv: </strong>{normalizedCsv.length} rows</p>
          <p><strong>symbol: </strong>{targetSymbol}</p>
          <p><strong>date: </strong>{targetDate || 'MM-DD-YYYY'}</p>
          <p><strong>tz: </strong>{targetTimezone}</p>
          <p><strong>side: </strong>{targetSide}</p>
        </div>
      </>
    ),
  }
}

export function createDataCallErrorToast(errMsg: string): IToastProps {
  return {
    timeout: 4000,
    intent: Intent.DANGER,
    message: (
      <>
        <div>
          <h4>Request Error</h4>
          <p>{errMsg}</p>
        </div>
      </>
    ),
  }
}
