import React, { ReactType } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AddToastAction, createAddToastAction } from '../../actions';

export function withToasts(WrappedComponent: ReactType){

  type Props = { } & DispatchProps

  class ToastHoc extends React.Component<Props>{

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      const {
        children,
        ...restProps
      } = this.props
      return (
        <WrappedComponent
          {...restProps}
          >
          {children}
        </WrappedComponent>
      )
    }
  }

  type DispatchProps = {
    addToast: AddToastAction
  }

  const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
    addToast: createAddToastAction(dispatch),
  })

  const ConnectedHoc = connect(
    null,
    mapDispatchToProps
  )(ToastHoc)

  return ConnectedHoc
}

export default withToasts