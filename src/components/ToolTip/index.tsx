import { Popover } from '@blueprintjs/core';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IApplicationState } from '../../reducers';
import { createToggleTooltipsAction, ToggleTooltipsAction } from '../../actions';



export function withToolTip<PassThroughProps>(
	WrappedComponent: React.ComponentType<PassThroughProps>,
	tooltipText: string,
): React.ComponentType<PassThroughProps> {

	type HocProps = {
	} & ConsumedStoreProps & DispatchProps

	type HocState = {

	}

	class ToolTipHoc extends React.Component<PassThroughProps & HocProps, HocState> {
	
		constructor(props: PassThroughProps & HocProps){
			super(props)
		}
	
		render() {
			const { 
				isTooltipActive,
				toggleTooltip,
				// props to pass down
				children,
				...passThroughProps
			} = this.props
			return (
				<div className="component-with-tooltip">
					<style jsx>{`
						.component-with-tooltip {
							width: 100%;
						}
						.popover-content-container {
							height: 100%;
							padding: 15px;
						}
					`}</style>
					<Popover
						minimal={false}
						isOpen={isTooltipActive}
						usePortal={true}
						hasBackdrop={true}
						wrapperTagName="div"
						onClose={() => toggleTooltip(false)}
						content={
							<div className="popover-content-container">
								<p>{tooltipText}</p>
							</div>
						}
						>
						<div style={{
								border: isTooltipActive ? '1px solid red' : 'none'
							}}>
							<WrappedComponent
								{...passThroughProps as PassThroughProps}
								>
								{children}
							</WrappedComponent>
						</div>
					</Popover>
				</div>
			)
		}
	}


  type ConsumedStoreProps = {
    isTooltipActive: boolean
  }

  const mapStateToProps = ({ appControls }: IApplicationState): ConsumedStoreProps => ({
    isTooltipActive: appControls.areToolTipsActive,
  })

	type DispatchProps = {
		toggleTooltip: ToggleTooltipsAction
	}

	const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
		toggleTooltip: createToggleTooltipsAction(dispatch),
	})

	const Connected = connect(
		mapStateToProps,
		mapDispatchToProps,
	)(ToolTipHoc)

	return Connected

}



