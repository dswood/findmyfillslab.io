import { Button, Intent, Popover, Position } from '@blueprintjs/core';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createSetDayStartAction, createSetSelectedFmfCsvData, createSetTopVisibleBeamAction, SetDayStartAction, SetSelectedFmfCsvData, SetTopVisibleBeamAction } from '../../actions';
import { IApplicationState, selectBeamsByKeys } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { FEGrainType } from '../../types';
import { getDateFromUtcUnix, getLast } from '../../util';
import DatePicker from '../DatePicker';
import GranularitySelector from '../GranularitySelector';



type Props = {
  className?: string
} & StoreProps & DispatchProps


class RangeSelector extends React.Component<Props>{

  componentWillReceiveProps(nextProps: Props) {
    const { selectedSymbolKey, selectedDayStartUtc } = nextProps
    if(
      selectedSymbolKey !== this.props.selectedSymbolKey
      || selectedDayStartUtc !== this.props.selectedDayStartUtc
    ){
      this.resetToLowestTimeGrain() // On symbol change or date change, resets to lowest grain
    }
  }

  handleDateChange = (date: Date) => {
    this.props.onDateSelect(date)
    this.props.setSelectedFmfCsvData('') // clear any rendered FMF fills data
  }

  resetToLowestTimeGrain(){
    const { activeTimeGrainKeys, possibleTimeGrains } = this.props
    const activeTimeGrains = selectBeamsByKeys(possibleTimeGrains, activeTimeGrainKeys)
    const lowestGrain = getLast(activeTimeGrains)
    this.props.selectTimeGranularity(
      possibleTimeGrains,
      activeTimeGrainKeys,
      lowestGrain.key, {
        riderStart: 0,
        riderSize: lowestGrain.beamSize,
      }
    )
  }

  renderTImeGrainChoices(){
    const { activeTimeGrainKeys, possibleTimeGrains, currentTimeGranularity } = this.props

    const activeTimeGrains = selectBeamsByKeys(possibleTimeGrains, activeTimeGrainKeys)

    return activeTimeGrains.slice().reverse().map((grain) => {
      const isCurrentGranularity = currentTimeGranularity === grain.key
      return (
        <Button
          key={grain.key}
          text={grain.label}
          intent={isCurrentGranularity ? Intent.PRIMARY : Intent.NONE}
          minimal={false}
          onClick={() => {
            if (!isCurrentGranularity) { // prevent firing action is button already clicked...
              this.props.selectTimeGranularity(
                possibleTimeGrains,
                activeTimeGrainKeys,
                grain.key
              )
            }
          }}
        />
      )
    })
  }

  render() {
    return (
      <>
        <Button
          icon="calendar"
          disabled={true}
          />
        <DatePicker
          onDateSelect={this.handleDateChange}
          dateValue={getDateFromUtcUnix(this.props.selectedDayStartUtc)}
          />
        <Popover
          position={Position.LEFT_BOTTOM}
          >
          <Button icon="settings" />
          <GranularitySelector/>
        </Popover>
        {this.renderTImeGrainChoices()}
      </>
    )
  }
}


type StoreProps = {
  possibleTimeGrains: INavBeam[]
  activeTimeGrainKeys: (FEGrainType)[]
  currentTimeGranularity: FEGrainType
  selectedSymbolKey: string
  selectedDayStartUtc: number
}

const mapStateToProps = ({navigator, appControls}: IApplicationState): StoreProps => ({
  // NOTE: multi-beam navigator interface carries time granularity information!
  possibleTimeGrains: navigator.navBeams,
  activeTimeGrainKeys: navigator.activeBeamKeys,
  selectedSymbolKey: appControls.selectedSymbolKey,
  currentTimeGranularity: navigator.topVisibleBeamKey,
  selectedDayStartUtc: navigator.selectedDayStartUtc,
})

type DispatchProps = {
  selectTimeGranularity: SetTopVisibleBeamAction
  setSelectedFmfCsvData: SetSelectedFmfCsvData
  onDateSelect: SetDayStartAction
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  selectTimeGranularity: createSetTopVisibleBeamAction(dispatch),
  setSelectedFmfCsvData: createSetSelectedFmfCsvData(dispatch),
  onDateSelect: createSetDayStartAction(dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RangeSelector);