import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AddToastAction, createFetchSymbolDataAction, FetchSymbolDataAction } from '../../actions';
import { DUR_TRADING_DAY } from '../../constants';
import { findBEGrainTypeForFEGrainType } from '../../records';
import { EMPTY_SYMBOL_KEY, IApplicationState, selectDataSliceKeys } from '../../reducers';
import { BEGrainType, FEGrainType } from '../../types';
import { createReqToast } from '../Toaster';


type Props = {
  addToast: AddToastAction
} & StoreProps & DispatchProps

let INFLIGHT_SYMBOL_DATA_URL = ''

class DataManager extends React.Component<Props> {

  static setInflightSymbolDataCallUrl = (url: string) => {
    console.log('inflight symbol data call:', url)
    INFLIGHT_SYMBOL_DATA_URL = url
  }
  static getInflightSymbolDataCallUrl = () => {
    return INFLIGHT_SYMBOL_DATA_URL
  }

  constructor(props: Props){
    super(props)
  }

  componentWillReceiveProps(nextProps: Props) {
    const { frontendGrainType, seriesDataTimeMin, seriesDataTimeMax,backendGrainType, selectedSymbolKey, selectedDayStartUtc } = nextProps;

    // NOTE: must check changes in order below, as those warranting new data calls are checked first...

    // 1. Check for SYMBOL change...
    if (selectedSymbolKey !== this.props.selectedSymbolKey) {
      // console.log(`%c DataManager: new selectedSymbolKey: ${selectedSymbolKey} `, Colors.LOG_DATA )
      this.handleSymbolChange(selectedSymbolKey)
      return
    }

    // 2. Check for DATE change...
    if (selectedDayStartUtc !== this.props.selectedDayStartUtc) {
      // console.log(`%c DataManager: new selectedDayStartUtc: ${selectedDayStartUtc} `, Colors.LOG_DATA )
      this.handleDateChange(selectedDayStartUtc)
      return
    }

    // 3. Check for GRANULARITY change...
    if (backendGrainType !== this.props.backendGrainType) {
      // console.log(`%c DataManager: new backendGrainType: ${backendGrainType} `, Colors.LOG_DATA )
      this.handleFeGrainChange(frontendGrainType, seriesDataTimeMin, seriesDataTimeMax)
      return
    }

    // 4. Check for TIME change...
    if (
      seriesDataTimeMin !== this.props.seriesDataTimeMin
      || seriesDataTimeMax !== this.props.seriesDataTimeMax
    ) {
      // console.log(`%c DataManager: new seriesDataTimeMin: ${seriesDataTimeMin} + new seriesDataTimeMax: ${seriesDataTimeMax} `, Colors.LOG_DATA )
      this.handleTimeChange(seriesDataTimeMin, seriesDataTimeMax)
      return
    }
  }

  componentWillUnmount(){
    DataManager.setInflightSymbolDataCallUrl('')
  }

  handleSymbolChange(newSymbol: string) {
    // Assumes all other props besides ones in arguments stays the same...
    const { allDataSlices, selectedDayStartUtc, backendGrainType } = this.props
    const viewStart = selectedDayStartUtc
    const viewEnd = selectedDayStartUtc + DUR_TRADING_DAY // On symbol change, always fetch 'min' granulartiy with entire day's set
    const eligibleDataKeys = selectDataSliceKeys(allDataSlices, newSymbol, backendGrainType, viewStart, viewEnd)
    if(eligibleDataKeys.length === 0){ // if NO existing data slices meet new criteria...
      this.requestNewData(
        FEGrainType.DAY,
        newSymbol,
        viewStart,
        viewEnd
      );
    }
  }

 handleTimeChange(newDataMin: number, newDataMax: number) {
    // Assumes all other props besides ones in arguments stays the same...
    const { selectedSymbolKey, frontendGrainType, backendGrainType, allDataSlices } = this.props

    const eligibleDataKeys = selectDataSliceKeys(allDataSlices, selectedSymbolKey, backendGrainType, newDataMin, newDataMax)

    if(eligibleDataKeys.length === 0){ // if NO existing data slices meet new criteria...
      this.requestNewData(
        frontendGrainType,
        selectedSymbolKey,
        newDataMin,
        newDataMax
      )
    }
  }

  handleDateChange(newDayStart: number) {
    const { selectedSymbolKey, frontendGrainType, backendGrainType, allDataSlices, seriesDataTimeMin, seriesDataTimeMax, selectedDayStartUtc } = this.props

    const newDataMin = seriesDataTimeMin - selectedDayStartUtc + newDayStart
    const newDataMax = seriesDataTimeMax - selectedDayStartUtc + newDayStart

    const eligibleDataKeys = selectDataSliceKeys(allDataSlices, selectedSymbolKey, backendGrainType, newDataMin, newDataMax)

    if(eligibleDataKeys.length === 0){ // if NO existing data slices meet new criteria...
      this.requestNewData(
        frontendGrainType,
        selectedSymbolKey,
        newDataMin,
        newDataMax
      )
    }
  }

  handleFeGrainChange(newFEGrainType: FEGrainType, newDataMin: number, newDataMax: number) {
    // Assumes all other props besides ones in arguments stays the same...
    const { selectedSymbolKey, allDataSlices } = this.props

    const eligibleDataKeys = selectDataSliceKeys(allDataSlices, selectedSymbolKey, findBEGrainTypeForFEGrainType(newFEGrainType), newDataMin, newDataMax)

    if(eligibleDataKeys.length === 0){ // if NO existing data slices meet new criteria...
      this.requestNewData(
        newFEGrainType,
        selectedSymbolKey,
        newDataMin,
        newDataMax
      );
    }
  }

  requestNewData(frontendGrainType: FEGrainType, symbolKey: string, viewStart: number, viewEnd: number){
    console.log()

    if(symbolKey !== EMPTY_SYMBOL_KEY){
      const backendGrainType = findBEGrainTypeForFEGrainType(frontendGrainType)
      if(!process.env.IS_PROD){
        this.props.addToast(createReqToast(
          backendGrainType,
          symbolKey,
          viewStart,
          viewEnd
        ))
      }
      this.props.fetchNewSymbolNewData(
        backendGrainType,
        symbolKey,
        viewStart,
        viewEnd
      )
    }
  }

  shouldComponentUpdate(){
    return false
  }

  render() {
    return null
  }
}


type StoreProps = {
  allDataSlices: Record<string, number[][]>
  selectedSymbolKey: string
  frontendGrainType: FEGrainType
  backendGrainType: BEGrainType
  selectedDayStartUtc: number
  seriesDataTimeMin: number
  seriesDataTimeMax: number
  isSpinnerActive: boolean
  timeZoneOffsetMin: number
}

const mapStateToProps = ({ navigator, appControls, symbolData }: IApplicationState): StoreProps => ({
  allDataSlices: symbolData,
  selectedSymbolKey: appControls.selectedSymbolKey,
  frontendGrainType: navigator.topVisibleBeamKey,
  backendGrainType: findBEGrainTypeForFEGrainType(navigator.topVisibleBeamKey),
  selectedDayStartUtc: navigator.selectedDayStartUtc,
  seriesDataTimeMin: navigator.selectedDayStartUtc + navigator.topLvlBeamStartAbs,
  seriesDataTimeMax: navigator.selectedDayStartUtc + navigator.topLvlBeamEndAbs,
  isSpinnerActive: appControls.isStockChartSpinnerActive,
  timeZoneOffsetMin: appControls.timeZoneOffsetMin,
})

type DispatchProps = {
  fetchNewSymbolNewData: FetchSymbolDataAction,
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  fetchNewSymbolNewData: createFetchSymbolDataAction(dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DataManager)
