
import { Alignment, Button, IconName, Intent, Menu, MenuItem, Popover, Position } from '@blueprintjs/core';
import React from 'react';


type Props = {
  options: { value: string, label: string }[]
  onSelect: ( value: string ) => void
  menuLabel: string
  menuIcon?: IconName
  menuRightIcon?: IconName
  menuIntent?: Intent
}

const MenuSelect = ({
  options = [],
  onSelect,
  menuLabel,
  menuIcon,
  menuRightIcon,
  menuIntent = Intent.NONE
}: Props) => (
  <Popover
    minimal={true}
    position={Position.BOTTOM_LEFT}
    content={
      <Menu>{
        options.map(({ value, label })=>(
          <MenuItem
            key={value}
            text={label}
            onClick={() => onSelect(value)}
            />
        ))
      }</Menu>
    }
    >
    <Button
      fill={true}
      intent={menuIntent}
      icon={menuIcon}
      rightIcon={menuRightIcon}
      alignText={Alignment.LEFT}
      text={menuLabel}
      />
  </Popover>
)


export default MenuSelect