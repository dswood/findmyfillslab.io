import React from 'react';
import config from '../../app.config';
import { Colors } from '../../constants';
import { BEGrainType, HcDataFormat, IOhlcBucket, SvgRectParam } from '../../types';
import { bucketScatterPlotData, deRezData, getRanks, rgbToRgba } from '../../util';


function createPtString(x: number, y:number){
  return x + ',' + y + ' '
}


export function createSvgPlinePts(seriesData: number[][], containerX: number, containerY: number, containerW: number, containerH: number){
  const xMin = seriesData[0][0]
  const xMax = seriesData[seriesData.length-1][0]
  let yMin = seriesData[0][1]
  let yMax = seriesData[0][1]
  seriesData.forEach(xy => {
    if (xy[1] < yMin) yMin = xy[1]
    if (xy[1] > yMax) yMax = xy[1]
  })
  const xRangeSize = xMax - xMin
  const yRangeSize = yMax - yMin
  const pxPerUnitX = containerW / xRangeSize
  const pxPerUnitY = containerH / yRangeSize


  const polylinePts = seriesData.map((xy) => [
    Math.floor(containerX + (xy[0] - xMin) * pxPerUnitX),
    Math.floor(containerH - (xy[1] - yMin) * pxPerUnitY),
  ])

  return polylinePts
}

export function createSvgPlineStringFromPts(polylinePts: number[][], containerX: number, containerY: number, containerW: number, containerH: number, shouldMakePolygon: boolean = false){
  const polylineString = polylinePts.reduce((prev, xy) => {
    prev += createPtString(xy[0], xy[1])
    return prev
  }, '')

  if(shouldMakePolygon){
    const ptBotLeft = createPtString(containerX, containerY + containerH)
    const ptBotRight = createPtString(containerX + containerW, containerY+containerH)
    return polylineString + ptBotRight + ptBotLeft
  } else {
    return polylineString
  }
}

export function createSvgRectParamsFromPts(
  containerW: number,
  containerH: number,
  buckets: IOhlcBucket[],
  globalPxHigh: number, // across all buckets
  globalPxLow: number, // across all buckets
  globalVolHigh: number, // across all buckets
  globalVolLow: number, // across all buckets
){
  // --------------------------
  // From the original array of buckets, filter out non-empty buckets into separate list and use it to create an array of sorted ranks (by bucket volume), this list late provides bucket volume ranking index to calculate proper annotation for the bucket
  const mapToNonEmptyBucketIndex = new Map<number, number>()
  let nonEmptyIndexCounter = 0,
  isNonEmptyBucket: boolean
  const nonEmptyBuckets = buckets.filter((bucket, originalIndex) => {
    isNonEmptyBucket = bucket.open !== undefined
    if(isNonEmptyBucket){
      mapToNonEmptyBucketIndex.set(originalIndex, nonEmptyIndexCounter)
      nonEmptyIndexCounter++
    }
    return isNonEmptyBucket
  })
  const bucketVolRanksAsc = getRanks<IOhlcBucket>(nonEmptyBuckets, (a, b) => b.volume - a.volume) // List of rank indexes (by volume) for all non-empty buckets
  const maxRank = bucketVolRanksAsc.length - 1


  // --------------------------
  // Convert ohlc bucket information into svg rect params
  const stepSizeX = containerW / 100
  const globalPxSpread = globalPxHigh - globalPxLow
  // console.log('ohlcBuckets ------>',buckets, globalPxHigh, globalPxLow, globalPxSpread, globalVolHigh, globalVolLow)
  const minBarH = 2 // height of smallest bar displayed
  const svgRectParams = buckets.reduce((prev: SvgRectParam[], bucket, i) => {
    const nonEmptyBucketIndex = mapToNonEmptyBucketIndex.get(i)
    if(nonEmptyBucketIndex !== undefined){


      // --------------------------
      // Calculate ohlc and backdrop bar color opacities based on bucket volume ranking...
      const bucketVolRank = bucketVolRanksAsc[nonEmptyBucketIndex]
      const backdropOpacity = 0.05 + bucketVolRank / maxRank * 0.3
      const barOpacity = 0.1 + bucketVolRank / maxRank * 0.9
      // console.log('bucketVolRank -->',bucket.volume, bucketVolRank)

      // --------------------------
      // Handle edge case where bucket spread is zero...
      let yOhlcBar: number, ohlcHeight: number
      if(globalPxSpread === 0){
        yOhlcBar = containerH / 2
        ohlcHeight = minBarH
      } else {
        yOhlcBar = (1 - (bucket.low - globalPxLow) / globalPxSpread) * containerH - minBarH/2
        ohlcHeight = (bucket.high - bucket.low) / globalPxSpread * containerH + minBarH // pad all bars by minBarH, and then offset back half way via "- minBarH/2" below...
      }


      // --------------------------
      // Create rect bar params...
      prev.push({ // Backdrop bar
        x: stepSizeX * i,
        y: 0,
        width: stepSizeX,
        height: containerH,
        fill: bucket.hasBlocks ?
          rgbToRgba(Colors.NAV_PREVIEW_OHLC_BAR_BLOCK, backdropOpacity)
            : rgbToRgba(Colors.NAV_PREVIEW_OHLC_BAR, backdropOpacity),
      }, { // Ohlc bar
        x: stepSizeX * i,
        y: yOhlcBar,
        width: stepSizeX,
        height: ohlcHeight,
        fill: bucket.hasBlocks ?
          rgbToRgba(Colors.NAV_PREVIEW_OHLC_BAR_BLOCK, barOpacity)
            : rgbToRgba(Colors.NAV_PREVIEW_OHLC_BAR, barOpacity),
      })
    }
    return prev
  }, [])

  return svgRectParams
}


type Props = {
  className?: string
  grainType: BEGrainType
  previewTimeMin: number
  previewTimeMax: number
  previewData: any[] | undefined
  previewDataFormat: HcDataFormat | ''
  x: number
  y: number
  w: number
  h: number
}

type State = {
  previewDataFormat: HcDataFormat | ''
  polylineString: string | undefined,
  polygonString: string | undefined,
  svgRectParams: any | undefined,
}

class PreviewGraph extends React.Component<Props, State>{

  constructor(props: Props) {
    super(props)
    this.state = {
      previewDataFormat: '',
      polylineString: '',
      polygonString: '',
      svgRectParams: undefined,
    }
  }

  componentDidMount(){
    const {previewData, previewDataFormat, previewTimeMin, previewTimeMax} = this.props
    this.buildGraphFromData(previewData, previewDataFormat, previewTimeMin, previewTimeMax)
  }

  componentWillReceiveProps(nextProps: Props){
    const { grainType, previewTimeMin, previewTimeMax, previewData, previewDataFormat, w } = nextProps
    if(
      w !== this.props.w
      || grainType !== this.props.grainType
      || previewDataFormat !== this.props.previewDataFormat
      || previewData !== this.props.previewData
      || previewTimeMin !== this.props.previewTimeMin
      || previewTimeMax !== this.props.previewTimeMax
    ){
      this.buildGraphFromData(previewData, previewDataFormat, previewTimeMin, previewTimeMax)
    }
  }

  buildGraphFromData(data: any[] | undefined, previewDataFormat: HcDataFormat | '', tMin: number, tMax: number){
    const {x, y, w, h } = this.props

    if(
      data === undefined
      || data.length === 0
    ){
      this.setState({
        previewDataFormat: '',
        polylineString: '',
        polygonString: '',
        svgRectParams: undefined,
      })
      return
    }

    switch (previewDataFormat) {

      case HcDataFormat.XY_ARRAY: {
        const trimStartIndex = data.findIndex(xy => xy[0] >= tMin)
        const trimEndIndex = data.findIndex(xy => xy[0] >= tMax)
        // console.log('trimStartIndex, trimEndIndex',trimStartIndex, trimEndIndex)
        const trimmedData = data.slice(trimStartIndex, trimEndIndex)
        const deRezedData = deRezData(trimmedData, config.NAV_SERIES_PREVIEW_MAX_REZ)
        const previewDataPts = createSvgPlinePts(
          deRezedData,
          0, 0, w, h,
        )
        const polylineString = createSvgPlineStringFromPts(
          previewDataPts,
          0, 0, w, h,
          false // shouldMakePolygon
        )
        const polygonString = createSvgPlineStringFromPts(
          previewDataPts,
          0, 0, w, h,
          true // shouldMakePolygon
        )
        this.setState({
          previewDataFormat,
          polylineString,
          polygonString,
          svgRectParams: undefined,
        })
      } break;

      case HcDataFormat.XY_OBJECT: {

        const trimStartIndex = data.findIndex(xy => xy.x >= tMin)
        const trimEndIndex = data.findIndex(xy => xy.x >= tMax)
        const trimmedData = data.slice(trimStartIndex, trimEndIndex)
        const bucketInfo =  bucketScatterPlotData(trimmedData, 100)

        // const bucketInfo =  bucketScatterPlotData(data, 100)
        if(bucketInfo === undefined) return

        const svgRectParams = createSvgRectParamsFromPts(
          w, h,
          bucketInfo.buckets,
          bucketInfo.globalPxHigh,
          bucketInfo.globalPxLow,
          bucketInfo.globalVolHigh,
          bucketInfo.globalVolLow,
        )

        this.setState({
          previewDataFormat,
          polylineString: '',
          polygonString: '',
          svgRectParams,
        })
      } break;
    }

  }

  renderGraph(){
    const { x, y, w, h } = this.props
    const { previewDataFormat, polylineString, polygonString, svgRectParams } = this.state

    switch (previewDataFormat) {
      case HcDataFormat.XY_ARRAY:
        return (
          <>
            <polyline
              className="preview-graph-area"
              fill={Colors.NAV_PREVIEW_AREA}
              stroke="none"
              points={polygonString}
              />
            <polyline
              className="preview-graph-polyline"
              fill="none"
              stroke={Colors.NAV_PREVIEW_PLINE}
              strokeWidth="0.5px"
              points={polylineString}
              />
          </>
        )

      case HcDataFormat.XY_OBJECT:
        return svgRectParams.map((rectParams, i)=>(
          <rect
            key={i}
            {...rectParams}
            />
        ))

      default:
        return (
          <text
            x={x + w/2}
            y={y + h/2}
            alignmentBaseline="middle"
            textAnchor="middle"
            fontSize="12px"
            fill={Colors.ACCENT_2}
            fontStyle="italic"
            >
            stale preview data, please reload...
          </text>
        )
    }
  }

  render(){
    const { className, x, y, w, h } = this.props

    return (
      <svg
        x={x}
        y={y}
        width={w}
        height={h}
        className={className || ''}
        >
        {this.renderGraph()}
      </svg>
    )
  }

}


export default PreviewGraph