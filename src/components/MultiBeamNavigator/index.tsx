import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createFetchSymbolDataAction, createMoveRiderStartEndAction, createSetNavAbsValsFromBeamAction, createSetNavigatorDimensions, FetchSymbolDataAction, IMoveRiderStartEndAction, SetNavigatorDimensions } from '../../actions';
import config from '../../app.config';
import { Colors } from '../../constants';
import { getDurationFormater } from '../../formatters';
import { findBEGrainTypeForFEGrainType, findPreviewSeriesTypeForFEGrainType } from '../../records';
import { getBeamBoundsAbs, getHcDataFormatFromKey, getRiderBoundsAbs, IApplicationState, INavLayout, selectBeamByKey, selectBeamLayoutParams, selectBeamsByKeys, selectDataSliceKeys, SymbolDataState } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { Dir, FEGrainType, NavUpdateType } from '../../types';
import { formatUnix } from '../../util';
import Beam from './Beam';
import SvgStepArrow from './SvgStepArrow';

export * from './util'



type Props = {
  containerW: number
} & StoreProps & DispatchProps

type State = {
  navigatorW: number
  navigatorH: number
}


class MultiBeamNavigator extends React.Component<Props, State>{

  container: HTMLDivElement | null

  constructor(props: Props) {
    super(props)
    this.container = null
    this.state = {
      navigatorW: 1000,
      navigatorH: 500,
    }
  }

  componentDidMount() {
    const { containerW } = this.props
    this.setNavigatorDimensions(containerW)
  }

  componentWillReceiveProps(nextProps: Props) {
    const { containerW } = nextProps

    if(containerW !== this.props.containerW){
      this.setNavigatorDimensions(containerW)
    }
  }

  setNavigatorDimensions(containerW: number){
    const { container } = this
    if(container !== null){
      this.props.setNavigatorDimensions(containerW, container.clientHeight)
    }
  }

  handleRiderStepLeftRight = (beamKey: FEGrainType, leftRight: Dir) => {
    const { topVisibleBeamKey, navBeams, activeBeamKeys} = this.props

    const beam = selectBeamByKey(navBeams, beamKey); if(beam === undefined) return;

    const { riderSize, riderStart, riderEnd } = beam
    const isTopVisibleBeam = beamKey === topVisibleBeamKey
    const plusMinus = leftRight === Dir.LEFT ? -1 : 1
    const pctStepping = isTopVisibleBeam ? config.PCT_RIDER_STEPPING : 1.0 // Stepping top level beam should increment rider by percentage of rider size so that there is some data context while moving left or right; on the other hand, when stepping action is called on a non top level beam, this action will likely send the chart view outside current data bounds anyways, for which the rendered context will be lost, thus the start/end value stepping in this case should step over current data bounds completely (ie. pctStepping = 1.0)...

    const shouldUpdateAbsBeamVals = isTopVisibleBeam ? false : false
    const shouldUpdateAbsRiderVals = isTopVisibleBeam ? true : false

    this.props.moveRiderStartEnd(
      NavUpdateType.RIDER_STEP_ARROW,
      navBeams,
      activeBeamKeys,
      beamKey,
      riderStart + plusMinus * riderSize * pctStepping,
      riderEnd + plusMinus * riderSize * pctStepping,
      shouldUpdateAbsBeamVals,
      shouldUpdateAbsRiderVals,
    )

    if (!isTopVisibleBeam){
      // On beam stepping, if event is generated from top level beam, the 'shouldUpdateAbsBeamVals' and 'shouldUpdateAbsRiderVals' above already updates navgiator value; but if a non top level beam is stepped left or right, then a separate implementation (below) is needed, as the previous step only updates beam states, and at this step, top level beam params will have been updated as part of upstream updates from this non-top level beam event, and so finally here the navigator absolute values needs to be baked in from the updated top level beam params...
      const topLevelBeam = selectBeamByKey(navBeams, topVisibleBeamKey)
      this.props.bakeInBeamVals(topLevelBeam)
    }
  }


  handleRiderBarMove = (beamKey: string, riderStart: number, riderEnd: number) => {
    this.handleRiderMove(beamKey, riderStart, riderEnd, NavUpdateType.RIDER_BAR_DRAG)
  }

  handleRiderHandleMove = (beamKey: string, riderStart: number, riderEnd: number) => {
    this.handleRiderMove(beamKey, riderStart, riderEnd, NavUpdateType.RIDER_HANDLE_DRAG)
  }


  handleRiderMove = (beamKey: string, riderStart: number, riderEnd: number, updateType: NavUpdateType) => {
    const { topVisibleBeamKey, navBeams, activeBeamKeys } = this.props
    const isTopVisibleBeam = beamKey === topVisibleBeamKey
    // Mouse drag itself should only update beam states with out committing to navigator absolute values, exception being if top level beam is being dragged, then should upate rider to immediate render out view change in chart...
    const shouldUpdateAbsBeamVals = false // do not fire data calls on move
    const shouldUpdateAbsRiderVals = isTopVisibleBeam ? true : false

    this.props.moveRiderStartEnd(
      updateType,
      navBeams,
      activeBeamKeys,
      beamKey,
      riderStart,
      riderEnd,
      shouldUpdateAbsBeamVals,
      shouldUpdateAbsRiderVals,
    )
  }

  handlRiderMoveEnd = (beamKey: string, isTopVisibleBeam: boolean) => {
    const { navBeams, topVisibleBeamKey } = this.props
    if (isTopVisibleBeam){
      //  if drag is on a non top level beam, the drag event itself always updates navigator values on the fly, thus at this point (on drag end) no action is needed...
    } else {
      // if drag is on a non top level beam, but the time drag end event is trigger, all nav beams states will have been updated, thus here we only need to extract the updated top level beam params and bake it into navigator's absolute values...
      const topLevelBeam = selectBeamByKey(navBeams, topVisibleBeamKey)
      this.props.bakeInBeamVals(topLevelBeam)
    }
  }

  getPreviewDataKey(beam: INavBeam): string | undefined {
    const { selectedDayStartUtc, selectedSymbolKey, symbolData } = this.props
    const previewDataSeriesType = findPreviewSeriesTypeForFEGrainType(beam.key)
    const backendGrainType = findBEGrainTypeForFEGrainType(beam.key)
    // Identify the sliceKey of the primary data series this beam represents...
    const absBeamVals = getBeamBoundsAbs(beam)
    // console.log('previewDataSeriesType',previewDataSeriesType)
    // console.log('backendGrainType',backendGrainType)

    return selectDataSliceKeys<string>(
      symbolData,
      selectedSymbolKey,
      backendGrainType,
      selectedDayStartUtc + absBeamVals.start,
      selectedDayStartUtc + absBeamVals.end,
      previewDataSeriesType,
    )[0] // takes first series to be preview data
  }

  fetchBeamPreviewData = (beam: INavBeam) => {
    const { selectedSymbolKey, selectedDayStartUtc } = this.props
    const absBeamVals = getBeamBoundsAbs(beam)
    this.props.fetchSymbolData(
      findBEGrainTypeForFEGrainType(beam.key),
      selectedSymbolKey,
      selectedDayStartUtc + absBeamVals.start,
      selectedDayStartUtc + absBeamVals.end,
    )
  }


  renderBeamWidgets(beam: INavBeam, isTopVisibleBeam: boolean){
    const { selectedSymbolKey, selectedDayStartUtc, navLayoutParams } = this.props
    const previewDataKey = this.getPreviewDataKey(beam)
    // time info values...
    const riderBounds = getRiderBoundsAbs(beam)
    const viewStartAbs = selectedDayStartUtc + riderBounds.start
    const viewEndAbs = selectedDayStartUtc + riderBounds.end
    const viewSize = viewEndAbs - viewStartAbs
    // time info texts...
    const viewStartText = moment(viewStartAbs).format('HH:mm:ss')
    const viewEndText = moment(viewEndAbs).format('HH:mm:ss')
    const viewSizeText = getDurationFormater(viewSize)(viewSize)

    const { stepArrowW } = navLayoutParams

    return (
      <div className="nav-beam-widgets-container"
        style={{
          paddingLeft: stepArrowW,
          paddingRight: stepArrowW,
        }}
        >
        <style jsx>{`

          .nav-beam-widgets-container {
            margin: 0;
            padding: 0px 9px;
            margin-bottom: 3px;
            display: flex;
          }
          .nav-beam-widgets-container .tag {
            margin-right: 4px;
            flex-grow: 0;
          }
          .nav-beam-widgets-container .load-preview-tag{
            margin-left: auto;
            margin-right: 0;
          }
        `}</style>
        <p className="tag beam-tag">
          {`${viewSizeText} / ${beam.label.toLowerCase()}`}
        </p>
        <p className="tag beam-tag">
          {`${viewStartText} - ${viewEndText}`}
        </p>
        <p className="tag widget-tag"
          onClick={() => {
            this.handleRiderBarMove(beam.key, 0, beam.riderSize)
            this.handlRiderMoveEnd(beam.key, isTopVisibleBeam)
          }}
          >
          pin view left
        </p>
        <p className="tag widget-tag"
          onClick={() => {
            this.handleRiderBarMove(beam.key, beam.beamSize - beam.riderSize, beam.beamSize)
            this.handlRiderMoveEnd(beam.key, isTopVisibleBeam)
          }}
          >
          pin view right
        </p>
        {
          isTopVisibleBeam ? (
            <p className="tag widget-tag"
              onClick={() => this.handleRiderHandleMove(beam.key, 0, beam.beamSize)}
              >
              full view
            </p>
          ) : null
        }
        {
          previewDataKey === undefined && selectedSymbolKey !== ''? (
            <p className="tag widget-tag load-preview-tag"
              onClick={() => this.fetchBeamPreviewData(beam)}
              >
              load preview
            </p>
          ) : null
        }
      </div>
    )
  }

  renderNavigatorBeams(){
    const { navBeams, visibleBeamKeys, shouldAllowLeftStepping, shouldAllowRightStepping, selectedDayStartUtc, navLayoutParams, symbolData } = this.props

    const { beamContainerW, beamContainerH, beamBarX, beamBarY, stepArrowW, stepArrowH, beamBarW } = navLayoutParams

    const visibleBeamsSet = selectBeamsByKeys(navBeams, visibleBeamKeys)

    return visibleBeamsSet.map((beam: INavBeam, index) => {
      const isTopVisibleBeam = index === 0
      const previewDataKey = this.getPreviewDataKey(beam)
      let previewHcDataFormat
      if(previewDataKey !== undefined){
        previewHcDataFormat = getHcDataFormatFromKey(previewDataKey)
      }

      // if(beam.key === 'MIN'){
      //   console.log('+++++++++++++++++++++++++++++++++')
      //   console.log('previewDataKey',previewDataKey)
      //   console.log('symbolData[previewDataKey]',symbolData[previewDataKey])
      //   console.log('+++++++++++++++++++++++++++++++++')
      // }

      return (
        <div className="nav-beam-container"
          key={beam.key}
          >
          {this.renderBeamWidgets(beam, isTopVisibleBeam)}
          <svg id="multi-beam-navigator-svg-container"
            width={beamContainerW}
            height={beamContainerH}
            >
            <SvgStepArrow
              dir="left"
              x={0}
              y={beamBarY}
              w={stepArrowW}
              h={stepArrowH}
              isDisabled={!shouldAllowLeftStepping}
              cornerRadius={stepArrowH * 0.1}
              xPad={stepArrowW * 0.40}
              yPad={stepArrowH * 0.30}
              strokeWidth="2px"
              stroke={Colors.NAV_STEPARROW}
              onClick={() => this.handleRiderStepLeftRight(beam.key, Dir.LEFT)}
              />
            <SvgStepArrow
              dir="right"
              x={beamBarX + beamBarW}
              y={beamBarY}
              w={stepArrowW}
              h={stepArrowH}
              isDisabled={!shouldAllowRightStepping}
              cornerRadius={stepArrowH * 0.1}
              xPad={stepArrowW * 0.40}
              yPad={stepArrowH * 0.30}
              strokeWidth="2px"
              stroke={Colors.NAV_STEPARROW}
              onClick={() => this.handleRiderStepLeftRight(beam.key, Dir.RIGHT)}
              />
            <Beam
              isTopVisibleBeam={isTopVisibleBeam}
              previewData={symbolData[previewDataKey]}
              previewDataFormat={previewHcDataFormat}

              onRiderMoveEnd={this.handlRiderMoveEnd}
              onRiderBarMove={this.handleRiderBarMove}
              onRiderHandleMove={this.handleRiderHandleMove}

              beamKey={beam.key}
              beamSize={beam.beamSize}
              beamStart={beam.beamStart}
              timeStartAbs={selectedDayStartUtc}
              tickIntervalSize={beam.tickIntervalSize}
              riderStart={beam.riderStart}
              riderSize={beam.riderSize}
              childBeamRiderStart={beam.childBeamRiderStart}
              childBeamRiderSize={beam.childBeamRiderSize}

              beamColor={beam.beamColor}
              riderColor={beam.riderColor}
              childBeamRiderColor={beam.childBeamRiderColor}
              {...navLayoutParams}
              />
          </svg>
        </div>
      )
    })
  }

  renderBeamParams(beam: INavBeam){
    // FOR DEV
    return (
      <>
        <p className="beam-rider-bounds"
          style={{ fontSize: '12px' }}
          >
          {`rider: ${formatUnix(Math.floor(beam.beamStart + beam.riderStart))} - ${formatUnix(Math.floor(beam.beamStart + beam.riderStart + beam.riderSize))} `}
        </p>
        <p className="beam-bounds"
          style={{ fontSize: '12px' }}
          >
          {`beam: ${formatUnix(Math.floor(beam.beamStart))} - ${formatUnix(Math.floor(beam.beamStart + beam.beamSize))} `}
        </p>
      </>
    )
  }

  renderNavParamSummary() {
    const { navBeams, visibleBeamKeys, selectedDayStartUtc } = this.props
    const visibleBeamsSet = selectBeamsByKeys(navBeams, visibleBeamKeys)

    const absRiderVals = getRiderBoundsAbs(visibleBeamsSet[0])
    const absBeamVals = getBeamBoundsAbs(visibleBeamsSet[0])
    return (
      <div>
        <h4>Current View:</h4>
        <p>dataGranularity: <strong>{visibleBeamsSet[0].key}</strong></p>
        <p>
          beam: <strong>{`${formatUnix(selectedDayStartUtc + absBeamVals.start)} - ${formatUnix(selectedDayStartUtc + absBeamVals.end)}`}</strong>
          &nbsp;/&nbsp;
          rider: <strong>{`${formatUnix(selectedDayStartUtc + absRiderVals.start)} - ${formatUnix(selectedDayStartUtc + absRiderVals.end)}`}</strong>
        </p>
        <p>
          beam size: <strong>{`${absBeamVals.size}`}</strong>
          &nbsp;/&nbsp;
          rider size: <strong>{`${absRiderVals.size}`}</strong>
        </p>
      </div>
    )
  }

  render() {
    // console.log('%c MultiBeamNavigator rendering...', 'color:red')
    return (
      <div
        id="multi-beam-navigator-container"
        ref={ el => this.container = el}
        >
        {this.renderNavigatorBeams()}
      </div>
    )
  }
}

type StoreProps = {
  browserW: number
  browserH: number
  navBeams: INavBeam[]
  activeBeamKeys: (FEGrainType)[]
  visibleBeamKeys: (FEGrainType)[]
  selectedDayStartUtc: number
  selectedSymbolKey: string
  symbolData: SymbolDataState
  topVisibleBeamKey: string
  shouldAllowLeftStepping: boolean
  shouldAllowRightStepping: boolean
  // INavLayout...
  navLayoutParams: INavLayout
}

const mapStateToProps = ({ navigator, appControls, symbolData }: IApplicationState): StoreProps => ({
  browserW: appControls.browserW,
  browserH: appControls.browserH,
  navBeams: navigator.navBeams,
  activeBeamKeys: navigator.activeBeamKeys,
  visibleBeamKeys: navigator.visibleBeamKeys,
  topVisibleBeamKey: navigator.topVisibleBeamKey,
  selectedDayStartUtc: navigator.selectedDayStartUtc,
  selectedSymbolKey: appControls.selectedSymbolKey,
  symbolData,
  shouldAllowLeftStepping: !navigator.isRiderAtNavStartAbs,
  shouldAllowRightStepping: !navigator.isRiderAtNavEndAbs,

  // INavLayout...
  navLayoutParams: selectBeamLayoutParams(navigator),
})

type DispatchProps = {
  bakeInBeamVals: (beam: INavBeam | undefined) => void
  moveRiderStartEnd: IMoveRiderStartEndAction
  fetchSymbolData: FetchSymbolDataAction
  setNavigatorDimensions: SetNavigatorDimensions
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  setNavigatorDimensions: createSetNavigatorDimensions(dispatch),
  bakeInBeamVals: (beam) => createSetNavAbsValsFromBeamAction(dispatch)(beam, true, true),
  moveRiderStartEnd: createMoveRiderStartEndAction(dispatch),
  fetchSymbolData: createFetchSymbolDataAction(dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MultiBeamNavigator);