import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createFetchSymbolDataAction, FetchSymbolDataAction } from '../../actions';
import { findBEGrainTypeForFEGrainType, findPreviewSeriesTypeForFEGrainType } from '../../records';
import { getHcDataFormatFromKey, IApplicationState, selectDataSliceKeys, SymbolDataState } from '../../reducers';
import { FEGrainType, HcDataFormat } from '../../types';


function getPreviewDataKey(
  beamKey: FEGrainType,
  beamStart: number,
  beamSize: number,
  selectedSymbolKey: string,
  selectedDayStartUtc: number,
  symbolData: SymbolDataState,
): string | undefined {
  const previewDataSeriesType = findPreviewSeriesTypeForFEGrainType(beamKey)
  const backendGrainType = findBEGrainTypeForFEGrainType(beamKey)
  // Identify the sliceKey of the primary data series this beam represents...
  return selectDataSliceKeys<string>(
    symbolData,
    selectedSymbolKey,
    backendGrainType,
    selectedDayStartUtc + Math.floor(beamStart),
    selectedDayStartUtc + Math.floor(beamStart + beamSize),
    previewDataSeriesType,
  )[0] // takes first series to be preview data
}


export function withPreviewData<PassThroughProps>(
  WrappedComponent: React.ComponentType<PassThroughProps>
): React.ComponentType<PassThroughProps> {

  type HocProps = {
    isTopVisibleBeam: boolean
    beamKey: FEGrainType,
    beamStart: number,
    beamSize: number,
  } & ConsumedStoreProps & DispatchProps

  type HocState = {
    previewDataKey: string | undefined | 'pending'
    previewData: any[] | undefined
    previewDataFormat: HcDataFormat | undefined
  }

  class BeamPreviewDataHOC extends React.Component<PassThroughProps & HocProps, HocState> {
    constructor(props: PassThroughProps & HocProps) {
      super(props);
      this.state = {
        previewDataKey: undefined,
        previewData: undefined,
        previewDataFormat: undefined,
      }
    }

    componentDidMount(){ }

    componentWillReceiveProps(nextProps: HocProps){
      this.managePreviewData(
        nextProps.beamKey,
        nextProps.beamStart,
        nextProps.beamSize,
        nextProps.selectedSymbolKey,
        nextProps.selectedDayStartUtc,
        nextProps.symbolData,
        nextProps.isTopVisibleBeam,
      )
    }

    managePreviewData(
      beamKey: FEGrainType,
      beamStart: number,
      beamSize: number,
      selectedSymbolKey: string,
      selectedDayStartUtc: number,
      symbolData: SymbolDataState,
      isTopVisibleBeam: boolean,
    ){
      const newPreviewDataKey = getPreviewDataKey(
        beamKey,
        beamStart,
        beamSize,
        selectedSymbolKey,
        selectedDayStartUtc,
        symbolData,
      )

      if(newPreviewDataKey !== undefined){
        // newPreviewDataKey reflects a existing key on symbolData store, thus if it exists, corresponding data must also exists...
        this.setState({
          previewDataKey: newPreviewDataKey,
          previewDataFormat: getHcDataFormatFromKey(newPreviewDataKey),
          previewData: symbolData[newPreviewDataKey]
        })
      } else { // if No preview data
        if(this.state.previewDataKey === 'pending'){
          // 'pending' indicates data calls already made, should not make state changes and continue to wait for data...
        } else {
          // a new valid previewDataKey is detected, should make data call and set pending status...
          if(!isTopVisibleBeam){ // only auto fetch preview data if beam is not the top most visible beam to prevent double firing data calls, since that beam's data is always update to date
            this.setState({
              previewDataKey: 'pending',
              previewDataFormat: undefined,
              previewData: undefined,
            }, () => {
              this.fetchBeamPreviewData(
                beamKey,
                beamStart,
                beamSize,
                selectedSymbolKey,
                selectedDayStartUtc,
              )
            })
          }
        }
      }
    }

    fetchBeamPreviewData = (
      beamKey: FEGrainType,
      beamStart: number,
      beamSize: number,
      selectedSymbolKey: string,
      selectedDayStartUtc: number,
    ) => {
      this.props.fetchSymbolData(
        findBEGrainTypeForFEGrainType(beamKey),
        selectedSymbolKey,
        selectedDayStartUtc + Math.floor(beamStart),
        selectedDayStartUtc + Math.floor(beamStart + beamSize),
      )
    }

    render() {
      const {
        // props to omit from children
        symbolData,
        selectedSymbolKey,
        selectedDayStartUtc,
        fetchSymbolData: fetchForNewData,
        // props to pass down
        children,
        ...passThroughProps
      } = this.props
      const {
        previewData,
        previewDataFormat,
      } = this.state

      return (
        <WrappedComponent
          previewData={previewData}
          previewDataFormat={previewDataFormat}
          {...passThroughProps as PassThroughProps}
          >
          {children}
        </WrappedComponent>
      )
    }
  }

  type ConsumedStoreProps = {
    symbolData: SymbolDataState
    selectedSymbolKey: string
    selectedDayStartUtc: number
  }

  const mapStateToProps = ({ navigator, symbolData, appControls }: IApplicationState): ConsumedStoreProps => ({
    symbolData,
    selectedSymbolKey: appControls.selectedSymbolKey,
    selectedDayStartUtc: navigator.selectedDayStartUtc,
  })

  type DispatchProps = {
    fetchSymbolData: FetchSymbolDataAction
  }

  const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
    fetchSymbolData: createFetchSymbolDataAction(dispatch),
  })

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(BeamPreviewDataHOC)

}