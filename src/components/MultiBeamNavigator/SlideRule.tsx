import moment from 'moment';
import React from 'react';

function _getTextAnchorPos(currentIndex: number, endIndex: number){
  return currentIndex == 0 ? 'start' // first
    : currentIndex === endIndex ? 'end' // last
      : 'middle' // in between
}

function _getPxVal(tVal: number, tSize: number, w: number) {
  return Math.floor(tVal / tSize * w)
}

type Props = {
  className?: string
  x: number
  w: number
  y: number
  ticksH: number
  ticksLabelH: number
  tickIntervalSize: number
  color: string
  maxTicksCount?: number
  tStart?: number // time min
  tSize?: number // time size (relative value)
}

const SlideRule = ({
  className = '',
  x,
  w,
  y,
  ticksH,
  ticksLabelH,
  tickIntervalSize,
  color,
  maxTicksCount = 8,
  tStart = 0,
  tSize =  60 * 1000,
}: Props) => {

  const ticksCount = Math.floor(tSize / tickIntervalSize)
  const nth = Math.ceil(ticksCount / maxTicksCount)

  const rulerTicks = []
  let tickStepVal: number, tickX: number, tickTime: number
  for (let i = 0; i <= ticksCount; i++) {
    if (i % nth === 0){ // keep only every nth...
      tickStepVal = tickIntervalSize * i
      tickX = x + _getPxVal(tickStepVal, tSize, w)

      tickTime = tStart + tickStepVal
      if (i === ticksCount) { // last tick at end of bar
        tickX = x + w
        tickTime = tStart + tSize
      }
      rulerTicks.push(
        <g key={i}>
          <line
            x1={tickX}
            x2={tickX}
            y1={y}
            y2={y + ticksH}
            stroke={color}
            />
          <text
            x={tickX}
            y={y + ticksH + 2}
            alignmentBaseline="hanging"
            textAnchor={_getTextAnchorPos(i, ticksCount)}
            fill={color}
            >
            {moment(tickTime).format('H:mm:ss')}
          </text>
        </g>
      )
    }
  }

  return (
    <g className={className}
      >
      {rulerTicks}
    </g>
  )
}






export default SlideRule