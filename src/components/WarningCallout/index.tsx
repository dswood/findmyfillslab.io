import { Callout, Code, Intent } from "@blueprintjs/core";
import React from 'react';
import { CsvConfigStatusTypes } from '../../types';
import { CsvParams } from '../CsvFileTable';

function makeJsxList(stringItems: string[], maxItemToRender?: number): JSX.Element[] {
  const itemsToRender = stringItems.slice(0, maxItemToRender)
  const itemsLeft = stringItems.slice(maxItemToRender)
  return itemsToRender.reduce((prev: JSX.Element[], strItem, i, array) => {
    prev.push(<Code key={strItem}>{strItem}</Code>)
    if(i < array.length - 1){ // not last
      prev.push(<span key={i}>,&nbsp;</span>)
    } else {
      if(maxItemToRender !== undefined && itemsLeft.length > 0){
        prev.push(<span key={i}>{` and ${itemsLeft.length} more items.`}</span>)
      } else {
        prev.push(<span key={i}>.</span>)
      }
    }
    return prev
  }, [])
}


interface Props {
  csvInfo: CsvParams | undefined
  status: keyof typeof CsvConfigStatusTypes | undefined
  className?: string
}

const WarningMessage = ({
  className = '',
  csvInfo,
  status,
}: Props) => {
  if(csvInfo === undefined) return null

  switch (status) {

    case CsvConfigStatusTypes.INCORRECT_SYMBOL_HEADER_MAPPING:
      return (
        <Callout
          className={className}
          icon="error"
          title="Invalid Symbol Mapping"
          intent={Intent.DANGER}
          >
          <span>No valid symbol value detected in your csv, please make sure your header mapping for <Code>Symbol</Code> is correct.</span>
        </Callout>
      )

    case CsvConfigStatusTypes.HAS_UNDEFINED_HEADER_MAPPINGS:
      return (
        <Callout
          className={className}
          icon="error"
          title="Undefined Mapping Detected"
          intent={Intent.DANGER}
          >
          <span>Please make sure there are no <Code>Undefined</Code> header mappings.</span>
        </Callout>
      )

    case CsvConfigStatusTypes.INCOMPLETE_HEADER_MAPPINGS:
      return (
        <Callout
          className={className}
          icon="error"
          title="Required Mapping Missing"
          intent={Intent.DANGER}
          >
          <span>Please make sure the following header mappings are accounted for:&nbsp;<span>{makeJsxList(csvInfo.missingHeaders)}</span></span>
        </Callout>
      )

    case CsvConfigStatusTypes.INVALID_DATE_SELECTION:
      return (
        <Callout
          className={className}
          icon="error"
          title="Invalid Date Configuration"
          intent={Intent.DANGER}
          >
          <span>Please make sure to specify a valid date that reflects the data in your csv.</span>
        </Callout>
      )

    case CsvConfigStatusTypes.MULTIPLE_SYMBOLS_IN_CSV:
      return (
        <Callout
          className={className}
          icon="warning-sign"
          title="Multiple Symbols in CSV"
          intent={Intent.WARNING}
          >
          <span>Following symbols are found in your CSV:&nbsp;<span>{makeJsxList(csvInfo.uniqueSymbolsInCsv, 3)}</span>, only one will be used. Please make sure to select the intented symbol below.</span>
        </Callout>
      )

    case CsvConfigStatusTypes.ALL_CONFIGS_VALID:
      return (
        <Callout
          className={className}
          icon="endorsed"
          title="All Configurations Valid"
          intent={Intent.SUCCESS}
          >
          <span>Your csv is ready to be uploaded!</span>
        </Callout>
      )

    default:
      return null
  }
}


export default WarningMessage