import { Button, ButtonGroup, Intent, Tag } from '@blueprintjs/core';
import moment from 'moment';
import React from 'react';
import { CsvParams } from '.';

type Props = {
  csvInfo: CsvParams
  csvIndex: number
  onFileRemove: (fileIndex: number) => void
  onFileEdit: (fileIndex: number) => void
  onUploadCsvToBackend: (fileIndex: number) => void
}


class FileTableRow extends React.PureComponent<Props> {
  render(){
    const { csvInfo, csvIndex, onFileRemove, onFileEdit, onUploadCsvToBackend } = this.props
    return (
      <div className="column-container bp3-card bp3-elevation-0 bp3-interactive"
        key={csvInfo.uploadTime}
        >
        <style jsx>{`
          .column-container {
            display: flex;
            padding: 10px;
            margin-bottom: 10px;
            align-items: center;
          }
          .column-left, .column-right {
            display: flex;
            flex-direction: column;
            padding-right: 10px;
            flex: 0 0 auto;
          }
          .column-middle {
            flex: 1 0 auto;
          }
        `}</style>
        <div className="column-left">
          <Button
            icon="edit"
            intent="primary"
            minimal={true}
            onClick={() => onFileEdit(csvIndex)}
            />
          <Button
            icon="cross"
            intent="danger"
            minimal={true}
            onClick={() => onFileRemove(csvIndex)}
            />
        </div>
        <div className="column-middle">
          <b>{csvInfo.fileName}</b>
          <div>{`${csvInfo.targetSymbol} - ${csvInfo.targetDate}`}</div>
          <div>{moment(csvInfo.uploadTime).format('YYYY MMM D - h:mma')}</div>

        </div>
        <div className="column-right">
          <ButtonGroup vertical={true} fill={true}>
            <Tag
              intent={csvInfo.isCsvConfigValid ? Intent.SUCCESS : Intent.DANGER}
              minimal={true}
              large={false}
              fill={true}
              >
              {csvInfo.isCsvConfigValid ? 'valid csv' : 'invalid csv'}
            </Tag>
            <Button
              text="find my fills!"
              intent="primary"
              minimal={false}
              fill={true}
              disabled={!csvInfo.isCsvConfigValid}
              large={false}
              onClick={() => onUploadCsvToBackend(csvIndex)}
              />
          </ButtonGroup>
        </div>
      </div>
    )
  }

}

export default FileTableRow