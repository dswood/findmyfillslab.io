import { Button, Intent, TextArea } from '@blueprintjs/core';
import React from 'react';
import { withSelectTextOnInputFocus } from '../../util';
import CsvPreviewTable from '../CsvPreviewTable';
import ImporterWarning from './ImporterWarning';
const PapaParse = require('papaparse/papaparse.min.js')


type Props = {
  onValidCsvCreated: (csvHeader: string[], csvBody: string[][], fileName: string) => void
}

type State = {
  textAreaValue: string
  parsedCsvBody: string[][] | undefined
  parsedCsvHeaders: string[] | undefined
  isPastedTextValidCsv: boolean
}


class PasteCsvWidget extends React.Component<Props, State> {

  el_textArea: HTMLTextAreaElement | null

  constructor(props: Props){
    super(props)
    this.el_textArea = null
    this.state = {
      textAreaValue: 'Paste csv here...',
      parsedCsvBody: undefined,
      parsedCsvHeaders: undefined,
      isPastedTextValidCsv: false,
    }
  }

  componentDidMount(){
    const { el_textArea } = this
    if(el_textArea !== null){
      withSelectTextOnInputFocus(el_textArea) // select all textarea content on focus
    }
  }

  handleCsvReaderError = () => {
    console.error('CsvReader something went wrong!')
  }

  handleTextAreaChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { value: text } = e.target
    if(text === ''){
      this.setState({
        isPastedTextValidCsv: false,
        textAreaValue: text,
      })
    } else {
      this.setState({
        isPastedTextValidCsv: true,
        textAreaValue: text,
      })
    }
  }

  parseTextToCsv = () => {
    const { textAreaValue } = this.state

    const {
      data: parsedCsv,
      errors,
      meta,
    } = PapaParse.parse(
      textAreaValue, {
        // https://www.papaparse.com/docs#config
        header: false,
        skipEmptyLines: 'greedy',
        error: this.handleCsvReaderError,
      }
    )

    if(parsedCsv.length < 2){
      this.setState({
        isPastedTextValidCsv: false,
      })
    } else {
      const [
        parsedCsvHeaders,
        ...parsedCsvBody
      ] = parsedCsv
      // console.log('parsedCsvHeaders', parsedCsvHeaders)
      // console.log('parsedCsvBody', parsedCsvBody)
      this.setState({
        parsedCsvBody,
        parsedCsvHeaders,
      })
    }
  }

  handleConfirmParsedCsv = () => {
    const { parsedCsvBody, parsedCsvHeaders } = this.state
    if(
      parsedCsvHeaders !== undefined
      && parsedCsvBody !== undefined
    ) {
      const fileName = 'pasted_csv'
      this.props.onValidCsvCreated(parsedCsvHeaders, parsedCsvBody, fileName)
    }
  }

  render(){
    const { textAreaValue, isPastedTextValidCsv, parsedCsvBody, parsedCsvHeaders } = this.state

    const hasParsedCsv = parsedCsvHeaders !== undefined && parsedCsvBody !== undefined

    return (
      <div className="dialog-content-container">
        <ImporterWarning/>
        <TextArea
          inputRef={(ref: HTMLTextAreaElement | null) => this.el_textArea = ref}
          growVertically={false}
          large={true}
          fill={true}
          intent={isPastedTextValidCsv ? Intent.SUCCESS : Intent.WARNING}
          onChange={this.handleTextAreaChange}
          value={textAreaValue}
          />
        <Button
          icon="th-derived"
          intent="primary"
          text="Parse CSV"
          minimal={true}
          disabled={!isPastedTextValidCsv}
          onClick={this.parseTextToCsv}
          />

        {
          hasParsedCsv ? (
            <>
              <CsvPreviewTable
                headerRow={parsedCsvHeaders}
                bodyRows={parsedCsvBody}
                columnW={100}
                />
              <Button
                icon="import"
                intent="primary"
                text="Import CSV"
                minimal={true}
                disabled={!isPastedTextValidCsv}
                onClick={this.handleConfirmParsedCsv}
                />
            </>
          ) : null
        }
      </div>
    )
  }
}

export default PasteCsvWidget