import React from 'react';
import { ICsv } from '../../types';
const PapaParse = require('papaparse/papaparse.min.js')


type Props = {
  className?: string
  isDisabled?: boolean | undefined
  onFileLoaded: (csv: ICsv, fileName: string) => void
  parserOptions: Record<string, any>
  uploadedFileName?: string
}

type State = {
  reader: FileReader | null
}

// based on https://github.com/nzambello/react-csv-reader
class CsvParser extends React.Component<Props, State> {

  el_form: null | HTMLFormElement

  constructor(props: Props){
    super(props)
    this.el_form = null
    this.state = {
      reader: null
    }
  }

  handleSelectFile = (e: React.FormEvent<HTMLInputElement>): void  => {
    const { onFileLoaded, parserOptions } = this.props
    console.log('handleSelectFile event', e)

    if(e.target.files !== null){
      const file = e.target.files[0]

      const reader = new FileReader() // https://developer.mozilla.org/en-US/docs/Web/API/FileReader

      reader.onload = (evt: ProgressEvent) => {
        if (evt.target !== null){
          const csvData = PapaParse.parse(
            evt.target.result,
            parserOptions || {},
          )
          if (this.el_form !== null) this.el_form.reset()

          onFileLoaded(csvData.data, file.name)
        }
      };
      reader.readAsText(file);

      this.setState({
        reader,
      })
    }
  }

  render(){
    const {
      className,
      isDisabled,
      uploadedFileName,
    } = this.props

    return (
      <form className={className || 'csv-input-form'}
        ref={el => this.el_form = el}
        >
        <label className="bp3-file-input">
          <input
            type="file"
            accept=".csv, text/csv"
            onChange={this.handleSelectFile}
            disabled={isDisabled}
            />
          <span className="bp3-file-upload-input">
            {uploadedFileName || 'Choose file...'}
            </span>
        </label>
      </form>
    )
  }

}

export default CsvParser

/*
     <FileInput
        disabled={isDisabled}
        text="Choose file..."
        onInputChange={this.handleSelectFile}
        hasSelection={false}
        />
< input
  className = { className || 'csv-input'}
  type = "file"
  accept = ".csv, text/csv"
  onChange = { this.handleSelectFile }
  /> */