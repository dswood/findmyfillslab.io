import moment from 'moment';
import config from '../../app.config';
import { timezonedDateToUtc } from '../../util';

export enum TimeStatus {
  VALID = 'VALID',
  ERROR_START_ONLY = 'ERROR_START_ONLY',
  ERROR_END_ONLY = 'ERROR_END_ONLY',
  ERROR_START_END = 'ERROR_START_END'
}

export function utcToTimePickerdDate(unixUtc: number, tz: string){
  // NOTE: Bp3 TimePicker always uses browser timezone, thus new Date() is used
  // specified time zone will be baked in...
  return new Date(moment.utc(unixUtc).tz(tz).format('LL HH:mm:ss.SSS'))
}


export function normalizeTimePickerDateForAppTz(timePickerDate: Date){
  // NOTE: Bp3 TimePicker does not have localization, and thus will interpret any Date object in client timezone regardless of user specified timezone setting (implemented app-wide in moment).
  const appTzOffset = moment().utcOffset() * -1 // user specified arbitrary timezone for display purposes in app
  const timePickerTzOffset = new Date().getTimezoneOffset() // timezone of the browser used by Bp3 TimePicker under the hood, which can not be modified
  const diffTimePickerTzAppTz = (appTzOffset - timePickerTzOffset) * 60 * 1000 // difference in ms between TimePicker timezone and the desired display timezone in the app
  const offsetedTimeInTimePickerTz = timePickerDate.valueOf() + diffTimePickerTzAppTz // The padded time to help make TimePicker display the app timezone in its own timezone. this value is for TimePicker display purposes only and its value does not reflects the true app utc time.
  // console.log('------------------------------')
  // console.log('newTime', timePickerDate)
  // console.log('newTime.valueof()', timePickerDate.valueOf())
  // console.log('appTzOffset', appTzOffset)
  // console.log('timePickerTzOffset', timePickerTzOffset)
  // console.log('diffTimePickerTzAppTz', diffTimePickerTzAppTz)
  // console.log('offseted new time unix', offsetedTimeInTimePickerTz)
  // console.log('offseted new time in client', new Date(offsetedTimeInTimePickerTz))
  // console.log('------------------------------')
  return new Date(offsetedTimeInTimePickerTz)
}

export function validateTimeAndGetStatus(
  startTime: Date,
  endTime: Date,
  minTimeAllowed: number,
  maxTimeAllowed: number,
  timeZoneString: string,
): TimeStatus {
  const utcStart = timezonedDateToUtc(startTime, timeZoneString)
  const utcEnd = timezonedDateToUtc(endTime, timeZoneString)

  let errorAtStart = false
  let errorAtEnd = false
  // Start time boundary check...
  if(utcStart < minTimeAllowed || utcStart >= maxTimeAllowed){
    errorAtStart = true
  }
  // End time boundary check...
  if(utcEnd <= minTimeAllowed || utcEnd > maxTimeAllowed) {
    errorAtEnd = true
  }
  // Start-End lock check
  if(utcEnd - utcStart < config.MIN_VIEW_SIZE_ALLOWED){
    errorAtStart = true
    errorAtEnd = true
  }

  if(errorAtStart && errorAtEnd){
    return TimeStatus.ERROR_START_END
  } else if(errorAtStart){
    return TimeStatus.ERROR_START_ONLY
  } else if(errorAtEnd){
    return TimeStatus.ERROR_END_ONLY
  } else {
    return TimeStatus.VALID
  }
}