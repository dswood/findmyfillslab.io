import { IToastProps } from '@blueprintjs/core';
import { Dispatch } from "redux";
import { getSymbolRefData } from '../models';
import { SpinnerHosts } from '../reducers';
import { IOption } from '../types';


// ----------------------
export const TOGGLE_TOOLTIPS = 'TOGGLE_TOOLTIPS'
// ----------------------
export type ToggleTooltipsAction = (isActive: boolean ) => void
export const createToggleTooltipsAction = (dispatch: Dispatch): ToggleTooltipsAction => (isActive) => dispatch({
  type: TOGGLE_TOOLTIPS,
  data: isActive,
})

// ----------------------
export const ACTIVATE_SPINNER = 'ACTIVATE_SPINNER'
// ----------------------
export type ActivateSpinnerAction = (spinnerHost: SpinnerHosts ) => void
export const createActivateSpinnerAction = (dispatch: Dispatch): ActivateSpinnerAction => (spinnerHost) => dispatch({
  type: ACTIVATE_SPINNER,
  data: spinnerHost,
})


// ----------------------
export const DEACTIVATE_SPINNER = 'DEACTIVATE_SPINNER'
// ----------------------
export type DeactivateSpinnerAction = (spinnerHost: SpinnerHosts ) => void
export const createDeactivateSpinnerAction = (dispatch: Dispatch): DeactivateSpinnerAction => (spinnerHost) => dispatch({
  type: DEACTIVATE_SPINNER,
  data: spinnerHost,
})


// ----------------------
export const POPULATE_SYMBOL_REF_DATA = 'POPULATE_SYMBOL_REF_DATA'
// ----------------------
export type PopulateSymbolRefDataAction = (symbolOptions: IOption[]) => void
export const createPopulateSymbolRefDataAction = (dispatch: Dispatch): PopulateSymbolRefDataAction => (symbolOptions) => dispatch({
  type: POPULATE_SYMBOL_REF_DATA,
  data: symbolOptions,
})


// ----------------------
export const ADD_TOAST = 'ADD_TOAST'
// ----------------------
export type AddToastAction = (toastProps: IToastProps) => void
export const createAddToastAction = (dispatch: Dispatch): AddToastAction => (toastProps) => dispatch({
  type: ADD_TOAST,
  data: toastProps,
})


// ----------------------
export const SET_TIMEZONE = 'SET_TIMEZONE'
// ----------------------
export type SetTimeZoneAction = (timezone: string) => void

export const createSetTimeZoneAction = (dispatch: Dispatch): SetTimeZoneAction => (timezone) => {
  dispatch({ // Activate spinner over stock chart window...
    type: SET_TIMEZONE,
    data: timezone,
  })
}


// ----------------------
export const SELECT_SYMBOL = 'SELECT_SYMBOL'
// ----------------------
export type SelectSymbolAction = (symbolKey: string) => void

export const createSelectSymbolAction = (dispatch: Dispatch): SelectSymbolAction => (symbolKey) => {
  dispatch({
    type: SELECT_SYMBOL,
    data: symbolKey,
  })
}


// ----------------------
export const SET_CLIENT_BROWSER_DIMENSIONS = 'SET_CLIENT_BROWSER_DIMENSIONS'
// ----------------------
export type SetClientBrowserDimensionsAction = (
  browserW: number,
  browserH: number
) => void

export const createSetClientBrowserDimensionsAction = (dispatch: Dispatch): SetClientBrowserDimensionsAction => (
  browserW,
  browserH
) => dispatch({
  type: SET_CLIENT_BROWSER_DIMENSIONS,
  data: { browserW, browserH },
})


// ----------------------
export type FetchSymbolRefDataAction = () => void
// ----------------------
export const createFetchSymbolRefData = (dispatch: Dispatch): FetchSymbolRefDataAction => () => {
  const LOCALSTORAGE_REF_DATA_KEY = 'stringifiedSymbOptns'
  const stringifiedSymbOptns = localStorage.getItem(LOCALSTORAGE_REF_DATA_KEY)
  // console.log('stringifiedSymbOptns ---> ', typeof stringifiedSymbOptns)
  if (stringifiedSymbOptns === null){
    return getSymbolRefData().then((symbolOptions: IOption[]) => {
      console.log('fetched symbolOptions[0]', symbolOptions[0]);
      console.log('saving symbol options to local storage...')
      localStorage.setItem(LOCALSTORAGE_REF_DATA_KEY, JSON.stringify(symbolOptions))
      createPopulateSymbolRefDataAction(dispatch)(symbolOptions)
    })
  } else {
    console.log('retrieving symbol options from local storage...')
    const symbolOptions = JSON.parse(stringifiedSymbOptns)
    createPopulateSymbolRefDataAction(dispatch)(symbolOptions)
  }
}