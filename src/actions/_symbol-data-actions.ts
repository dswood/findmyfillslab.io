import { Dispatch } from "redux";
import { createAddToastAction } from '../actions';
import config from '../app.config';
import DataManager from '../components/DataManager';
import { createDataCallErrorToast } from '../components/Toaster';
import { Colors } from '../constants';
import { createSymbolDataApiUrl, getSymbolData } from "../models";
import { makeDataSliceKey, SpinnerHosts } from "../reducers";
import { BEDataMode, BEDataType, BEGrainType, HcAreaRangeData, HcScatterData, HcSeriesType, SymbolDataPayload } from "../types";
import { createActivateSpinnerAction, createDeactivateSpinnerAction } from './_app-controls-actions';
import { getScatterPlotMarkerRadius } from './_user-fills-actions';


// ----------------------
export const SET_SYMBOL_DATA = 'SET_SYMBOL_DATA'
// ----------------------
export type SetSymbolDataAction = (dataSlicesUnderSymbol: Record<string, number[][]>) => void

export const createSetSymbolDataAction = (dispatch: Dispatch): SetSymbolDataAction => (
  dataSlicesUnderSymbol
) => dispatch({
  type: SET_SYMBOL_DATA,
  data: dataSlicesUnderSymbol
})


// ----------------------
// Complex Action
// ----------------------
export type FetchSymbolDataAction = (
  backendGrainType: BEGrainType,
  symbolKey: string,
  viewStart: number,
  viewEnd: number
) => void

export const createFetchSymbolDataAction = (dispatch: Dispatch): FetchSymbolDataAction => (
  backendGrainType,
  symbolKey,
  viewStart,
  viewEnd
) => {

  createActivateSpinnerAction(dispatch)(SpinnerHosts.STOCK_CHART)

  const symbolDataApiUrl = createSymbolDataApiUrl(backendGrainType, symbolKey, viewStart, viewEnd);
  if(symbolDataApiUrl === DataManager.getInflightSymbolDataCallUrl()){
    return;
  } else {
    console.log(`%c --- Fetch symbol data for ${symbolKey} ---> `, 'background:teal;color:white')
    DataManager.setInflightSymbolDataCallUrl(symbolDataApiUrl)
  }

  getSymbolData(
    symbolDataApiUrl
  ).then(payload => {

    createDeactivateSpinnerAction(dispatch)(SpinnerHosts.STOCK_CHART)

    DataManager.setInflightSymbolDataCallUrl('')

    if(payload instanceof Error){
      // UN-SUCCESSFUL data call...
      createAddToastAction(dispatch)(
        createDataCallErrorToast(payload.message)
      )
    } else {

      const data = {
        sym: payload.sym,
        mode: payload.mode.toUpperCase(),
        start: Number(payload.start),
        end: Number(payload.end),
        type: payload.type.toUpperCase(),
        ticks: payload.ticks,
        nbbo: payload.nbbo,
      } as SymbolDataPayload

      // SUCCESSFUL data call...
      const dataSlicesUnderSymbol = normalizeSymbolDataToHcSeries(data)
      if(dataSlicesUnderSymbol !== undefined){
        createSetSymbolDataAction(dispatch)(dataSlicesUnderSymbol)// Push fetched data to store...
      }
    }
  })
}


function normalizeSymbolDataToHcSeries(data: SymbolDataPayload){
  const { sym, mode, start, type, end, ticks, nbbo } = data
  HcSeriesType
  switch (mode.toUpperCase()) {
    case BEDataMode.OHLC: {
      // ticks = [ts, o, h, l, c, v]
      const sliceKeyOhlcLine = makeDataSliceKey(sym, type, start, end, HcSeriesType.CANDLESTICK, BEDataType.TICKS)
      const sliceKeyOhlcVolume = makeDataSliceKey(sym, type, start, end, HcSeriesType.COLUMN, BEDataType.VOLUME)
      const seriesOhlcLine = ticks
      const seriesOhlcVolume: number[][] = []
      ticks.forEach(xy => {
        seriesOhlcVolume.push([xy[0], xy[5]])
      })
      return {
        [sliceKeyOhlcLine]: seriesOhlcLine,
        [sliceKeyOhlcVolume]: seriesOhlcVolume,
      }
    }
    case BEDataMode.TICKS: {
      // ------------------------
      // nbbo = [bid, ask, bid-sz, ask-sz, bid-ex, ask-ex]
      const seriesAreaRangeNbbo: HcAreaRangeData[] = []
      const seriesAreaRangeNbboKey = makeDataSliceKey(sym, type, start, end, HcSeriesType.AREARANGE, BEDataType.NBBO)
      if(nbbo !== undefined){
        nbbo.forEach(([ts, pxBid, pxAsk, szBid, szAsk, exBid, exAsk]) => {
          seriesAreaRangeNbbo.push({
            x: ts,
            low: pxBid,
            high: pxAsk,
            name: 'NBBO', // accessible via point.key in tooltip formatter
            szBid, exBid,
            szAsk, exAsk,
          })
        })
      }

      // ------------------------
      // ticks = [ts, px, sz, flags]
      // flags is a string containing any combination of `p`, `b` or `v`. `p` = last-sale-eligible (always present), `b` = block trade, `v` = volume-eligible (don't think you will need `p` or `v` for charting for now)
      const seriesScatterTicks: HcScatterData[] = []
      const seriesScatterTicksKey = makeDataSliceKey(sym, type, start, end, HcSeriesType.SCATTER, BEDataType.TICKS)

      let isBlockTrade = false
      ticks.forEach(([x, y, sz, flag]) => {
        const markerR = getScatterPlotMarkerRadius(
          sz,
          config.TICKS_SIZE_LOW,
          config.TICKS_SIZE_HIGH,
          config.TICKS_DATA_SCATTER_NODE_RADIUS_MIN,
          config.TICKS_DATA_SCATTER_NODE_RADIUS_MAX,
        )

        isBlockTrade = String(flag).indexOf('b') !== -1
        seriesScatterTicks.push({
          x, y,
          name: `${sym}: ${sz} shares`, // accessible via point.key in tooltip formatter
          labelrank: sz, // The rank for this point's data label in case of collision. If two data labels are about to overlap, only the one with the highest labelrank will be drawn.
          isBlockTrade,
          marker: {
            radius: markerR,
            fillColor: isBlockTrade ? Colors.HC_SCATTER_BLOCKS : Colors.HC_SCATTER_TRADES
          }
        })
      })

      return {
        [seriesAreaRangeNbboKey]: seriesAreaRangeNbbo,
        [seriesScatterTicksKey]: seriesScatterTicks,
      }
    }
  }

}
