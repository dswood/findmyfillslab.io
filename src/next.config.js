const withTypescript = require('@zeit/next-typescript') // https://github.com/zeit/next-plugins/tree/master/packages/next-typescript
const withCSS = require('@zeit/next-css')
const withSass = require('@zeit/next-sass')

const CONFIG = {
  env: {
    IS_PROD: process.env.BABEL_ENV === 'prod',
    ENV: process.env.BABEL_ENV,
  },
  exportPathMap() {
    return {
     '/': { page: '/'} ,
     '/test-csv': { page: '/test/csv'} ,
     '/test-login': { page: '/test/login'} ,
     '/privacy': { page: '/privacy-policy'} ,
     '/disclaimer': { page: '/disclaimer'} ,
    }
  },
  cssLoaderOptions: {
    url: false
  }
}



module.exports = (
  withTypescript(
    withSass(
      withCSS(CONFIG)
    )
  )
)

