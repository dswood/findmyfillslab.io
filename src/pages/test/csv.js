import '@blueprintjs//icons/lib/css/blueprint-icons.css';
import { FocusStyleManager } from '@blueprintjs/core';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import '@blueprintjs/table/lib/css/table.css';
import 'normalize.css/normalize.css';
import React from 'react';
import 'react-select/dist/react-select.css';
// import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import Toaster from '../../components/Toaster';
import CsvLoaderParser from '../../containers/CsvLoaderParser';
import globalStyles from '../../styles/global';

FocusStyleManager.onlyShowFocusOnTabs(); // prevent input component will rendering outline on focus

export default () => (
  <>
    <title>Find My Fills</title>
    <div id="root-container">
      <style jsx global>{globalStyles}</style>
      <div style={{
          width: '380px',
          margin: '0 auto',
        }}>
        <h1>Find My Fills Csv Uploader</h1>
        <CsvLoaderParser
          enableDevOnlyUrlInput={true}
          />
      </div>
      <Toaster />
    </div>
  </>
)
