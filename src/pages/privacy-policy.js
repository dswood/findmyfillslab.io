import 'normalize.css/normalize.css';
import globalStyles from '../styles/global';


const PrivacyPolicy = () => (
	<>
		<title>FMF: Privacy Policy</title>
		<div className="legal-doc-container">
			<style jsx global>{globalStyles}</style>
			<div className="doc-title">
				<h2>Privacy Policy</h2>
				<p>
					<i>PROOF TRADING, INC.</i><br/>
					<i>Last Updated: 8/2/2019</i>
				</p>
			</div>
			<div className="doc-content">
				<p>This privacy policy ("<b>Policy</b>") describes how Proof Trading, Inc. and its related companies ("<b>Company</b>") collect, use and share personal information of consumer users of this website, www.findmyfills.com (the "<b>Site</b>"). This Policy also applies to any of our other websites that post this Policy. This Policy does not apply to websites that post different statements.</p>


				<h3>WHAT WE COLLECT</h3>

				<p>We get information about you in a range of ways:</p>
				<ul>
					<li><b>Information You Give Us.</b> We collect your name, email address as well as other information you directly give us on our Site.</li>
					<li><b>Information We Get From Others.</b> We may get information about you from other sources. We may add this to information we get from this Site.</li>
					<li><b>Information Automatically Collected.</b> We automatically log information about you and your computer. For example, when visiting our Site, we log your IP address, computer operating system type, browser type, browser language, pages you viewed and information about your use of and actions on our Site.</li>
					<li><b>Cookies.</b> We may log information using "cookies." Cookies are small data files stored onyour hard drive by a website. We may use both session Cookies (which expire once you close your web browser) and persistent Cookies (which stay on your computer until you delete them) to provide you with a more personal and interactive experience on our Site.This type of information is collected to make the Site more useful to you and to tailor the experience with us to meet your special interests and needs</li>
				</ul>


				<h3>USE OF PERSONAL INFORMATION</h3>

				<p>We use your personal information as follows:</p>
				<ul>
					<li>We use your personal information to operate, maintain, and improve our sites, products, and services.</li>
					<li>We use your personal information to respond to comments and questions and provide customer service.</li>
					<li>We use your personal information to send information including confirmations, invoices, technical notices, updates, security alerts, and support and administrative messages.</li>
					<li>We use your personal information to communicate about promotions, upcoming events, and other news about products and services offered by us and our selected partners.</li>
					<li>We use your personal information to protect, investigate, and deter against fraudulent, unauthorized, or illegal activity.</li>
					<li>We use your personal information to provide and deliver products and services customers request.</li>
				</ul>


				<h3>SHARING OF PERSONAL INFORMATION</h3>

				<p>We may share personal information as follows:</p>
				<ul>
					<li>We may share personal information with your consent. For example, you may let us share personal information with others for their own marketing uses. Those uses will be subject to their privacy policies.</li>
					<li>We may share personal information when we do a business deal, or negotiate a business deal, involving the sale or transfer of all or a part of our business or assets. These deals can include any merger, financing, acquisition, or bankruptcy transaction or proceeding.</li>
					<li>We may share personal information for legal, protection, and safety purposes.
						<ul>
							<li>We may share information to comply with laws.</li>
							<li>We may share information to respond to lawful requests and legal processes.</li>
							<li>We may share information to protect the rights and property of Proof Trading, Inc., our agents, customers, and others. This includes enforcing our agreements, policies, and terms of use.</li>
							<li>We may share information in an emergency. This includes protecting the safety of our employees and agents, our customers, or any person.</li>
						</ul>
					</li>
					<li>We may share information with those who need it to do work for us.</li>
					<li>We may also share aggregated and/or anonymized data with others for their own uses.</li>
				</ul>


				<h3>INFORMATION CHOICES AND CHANGES</h3>

				<p>Our marketing emails tell you how to “opt-out.” If you opt out, we may still send you non-marketing emails. Non-marketing emails include emails about your accounts and our business dealings with you.</p>
				<p>You may send requests about personal information to our Contact Information below. You can request to change contact choices, opt-out of our sharing with others, and update your personal information.</p>
				<p>You can typically remove and reject cookies from our Site with your browser settings. Many browsers are set to accept cookies until you change your settings. If you remove or reject our cookies, it could affect how our Site works for you.</p>


				<h3>CONTACT INFORMATION</h3>

				<p>We welcome your comments or questions about this privacy policy. You may also contact us at our address:</p>
				<p>
					<span><i>Proof Trading, Inc.</i></span><br/>
					<span><i>20 Pine Street #2707</i></span><br/>
					<span><i>NEW YORK, New York 10005</i></span>
				</p>


				<h3>CHANGES TO THIS PRIVACY POLICY</h3>

				<p>We may change this privacy policy. If we make any changes, we will change the Last Updated date above.</p>

			</div>
		</div>
	</>
)

export default PrivacyPolicy