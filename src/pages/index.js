import React from 'react';
import App from '../containers/App';
import { withLogin } from '../containers/FirebaseLogin';

let AppWithLogIn
if(process.env.ENV === 'dev'){
  AppWithLogIn = App
} else {
  AppWithLogIn = withLogin(App)
}

export default () => (
  <>
		<title>Find My Fills</title>
    <AppWithLogIn/>
  </>
)

