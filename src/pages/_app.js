/*
a couple things to note about _app.js:

1. It is rendered once on the server - it is the root of your app

2. App's getInitialProps is called before anything else - even before getInitialProps in _document.js

3. It is created and mounted ONLY ONCE on the client - when the page first loads.Thereafter, on page navigation, App.getInitialProps is called before each page, and then its render function is called, to render the incoming page.

4. It lives for the entire lifetime of the clientside app.Unlike pages, it is not unmounted - ever.This makes it a great place to store data that should live for the entire app lifetime.

5. It sees all of the props passed to every page - and has the chance to modify them.
*/
import withRedux from 'next-redux-wrapper'; // https://github.com/kirill-konshin/next-redux-wrapper
import NextApp, { Container } from 'next/app';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import reducer from '../reducers';

import Router from 'next/router'
import * as gtag from '../../lib/gtag'
Router.events.on('routeChangeComplete', url => gtag.pageview(url))


class MyApp extends NextApp {

  static async getInitialProps({ Component: PageComponent, ctx }) {
    // component will read from store's state when rendered
    ctx.store.dispatch({ type: 'FOO', payload: 'foo' });
    // pass some custom props to component from here
    const pageProps = PageComponent.getInitialProps ? await PageComponent.getInitialProps(ctx) : {};
    return { pageProps };
  }

  render() {
    const { Component: PageComponent, pageProps, store } = this.props
    return (
      <Container>
        <Provider store={store}>
          <PageComponent {...pageProps} />
        </Provider>
      </Container>
    )
  }
}

/**
* @param {object} initialState
* @param {boolean} options.isServer indicates whether it is a server side or client side
* @param {Request} options.req NodeJS Request object (not set when client applies initialState from server)
* @param {Request} options.res NodeJS Request object (not set when client applies initialState from server)
* @param {boolean} options.debug User-defined debug mode param
* @param {string} options.storeKey This key will be used to preserve store in global namespace for safe HMR 
*/
const makeStore = (initialState, options) => {
  // console.log('------------------------')
  // console.log(options)
  // console.log('------------------------')
  const middlewares = []
  if (process.env.NODE_ENV !== 'production') {
    const logger = createLogger({
      collapsed: true,
      // predicate: (getState, action) => action.type !== UPDATE_NAV_BEAMS_SET
    })
    middlewares.push(logger)
  }
  const store = createStore(
    reducer,
    initialState,
    applyMiddleware.apply(null, middlewares)
  )
  return store
};


export default withRedux(makeStore, { debug: false })(MyApp)