import 'normalize.css/normalize.css';
import globalStyles from '../styles/global';

const Disclaimer = () => (
	<>
		<title>FMF: Disclaimer</title>
		<div className="legal-doc-container">
			<style jsx global>{globalStyles}</style>
			<div className="doc-title">
				<h2>Disclaimer</h2>
				<p>
					<i>PROOF TRADING, INC.</i><br/>
					<i>Last Updated: 8/2/2019</i>
				</p>
			</div>
			<div className="doc-content">
				<p>Proof does not make any guarantees, representations or warranties as to, and shall have no liability for, the timeliness, truthfulness, sequence, quality, completeness, accuracy, validity or freedom from interruption of any information or data on the FindMyFills Website. The content on the FindMyFills Website is not to be construed as a recommendation or offer to buy or sell or the solicitation of an offer to buy or sell any security, financial product or instrument; or to participate in any particular trading strategy. By using this service, you understand and acknowledge that any information on the website is intended to provide you with a reference point only, rather than as a basis for making trading decisions.</p>
			</div>
		</div>
	</>
)

export default Disclaimer