// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

// ./pages/_document.js
import Document, { Html, Head, Main, NextScript } from 'next/document';
import config from '../app.config';


class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    const isProduction = process.env.IS_PROD
    return { ...initialProps, isProduction };
  }

  setGoogleTags() { 
    // Inject script contents onto page
    return {
      __html: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '${config.GA_ID}');
      `
    };
  }

  render() {
    const { isProduction } = this.props;

    return (
      <Html>
        <Head>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
          <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"/>

          <link rel="apple-touch-icon" sizes="180x180" href="/static/assets/favicons/apple-touch-icon.png"/>
          <link rel="icon" type="image/png" sizes="32x32" href="/static/assets/favicons/favicon-32x32.png"/>
          <link rel="icon" type="image/png" sizes="16x16" href="/static/assets/favicons/favicon-16x16.png"/>
          <link rel="manifest" href="/static/assets/favicons/site.webmanifest"/>
          <link rel="mask-icon" href="/static/assets/favicons/safari-pinned-tab.svg" color="#5bbad5"/>
          <meta name="msapplication-TileColor" content="#da532c"/>
          <meta name="theme-color" content="#ffffff"/>

          {config.STYLESHEETS.map((href, i) => <link key={i} type="text/css" rel="stylesheet" href={href}/>)}
          {config.SCRIPTS.map((src, i) => <script key={i} src={src}/>)}

          {isProduction && (
            <>
              <script async src={`https://www.googletagmanager.com/gtag/js?id=${config.GA_ID}`}/>
              <script dangerouslySetInnerHTML={this.setGoogleTags()}/>
            </>
          )}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;