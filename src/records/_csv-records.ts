
export function findLabelForAllowedHeaderKey(headerKey: CsvMapperHeaders){
  return MAP_CSV_HEADER_KEY_TO_LABEL[headerKey]
}

export function findAllowedHeaderKeyForUserCsvColKey(userCsvColKey: string){
  return MAP_USER_CSV_COL_KEY_TO_ALLOWED_HEADER[userCsvColKey]
}

export enum CsvMapperHeaders {
  UNDEFINED = 'UNDEFINED',
  TIME = 'TIME',
  SYMBOL = 'SYMBOL',
  SIZE = 'SIZE',
  PRICE = 'PRICE',
  SIDE = 'SIDE',
  VENUE = 'VENUE',
}

const MAP_USER_CSV_COL_KEY_TO_ALLOWED_HEADER: {
  [key: string]: CsvMapperHeaders
} = {
  // --> Time...
  'DATE': CsvMapperHeaders.TIME,
  'TIME': CsvMapperHeaders.TIME,
  'DATETIME': CsvMapperHeaders.TIME,
  'TIMESTAMP': CsvMapperHeaders.TIME,
  'MILLI': CsvMapperHeaders.TIME,
  'TRADETIME': CsvMapperHeaders.TIME,
  // --> 'Symbol'
  'SYMBOL': CsvMapperHeaders.SYMBOL,
  'SYMB': CsvMapperHeaders.SYMBOL,
  'SYM': CsvMapperHeaders.SYMBOL,
  'TICKER': CsvMapperHeaders.SYMBOL,
  // --> 'Size'
  'SIZE': CsvMapperHeaders.SIZE,
  'SHARES': CsvMapperHeaders.SIZE,
  'LASTSIZE': CsvMapperHeaders.SIZE,
  'LASTSHARES': CsvMapperHeaders.SIZE,
  'QUANTITY': CsvMapperHeaders.SIZE,
  // --> 'Price'
  'PRICE': CsvMapperHeaders.PRICE,
  'PX': CsvMapperHeaders.PRICE,
  'LASTPRICE': CsvMapperHeaders.PRICE,
  'LASTPX': CsvMapperHeaders.PRICE,
  // --> 'Side'
  'SIDE': CsvMapperHeaders.SIDE,
  'BS': CsvMapperHeaders.SIDE,
  // --> 'Venue'
  'VENUE': CsvMapperHeaders.VENUE,
  'MARKET': CsvMapperHeaders.VENUE,
  'LASTMARKET': CsvMapperHeaders.VENUE,
}

const MAP_CSV_HEADER_KEY_TO_LABEL: {
  [k in CsvMapperHeaders]: string
} = {
  [CsvMapperHeaders.UNDEFINED]: 'Undefined',
  [CsvMapperHeaders.TIME]: 'Time',
  [CsvMapperHeaders.SYMBOL]: 'Symbol',
  [CsvMapperHeaders.SIZE]: 'Size',
  [CsvMapperHeaders.PRICE]: 'Price',
  [CsvMapperHeaders.SIDE]: 'Side',
  [CsvMapperHeaders.VENUE]: 'Venue',
}