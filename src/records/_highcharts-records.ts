import { BEGrainType, FEGrainType, HcDataFormat, HcSeriesType } from '../types';
import { findBEGrainTypeForFEGrainType } from './_granularity-records';


export function findHcDataFormatForHcSeriesType(hcSeriesType: HcSeriesType){
  return MAP_HC_SERIES_TYPE_TO_HC_DATA_FORMAT[hcSeriesType]
}

export function findPreviewSeriesTypeForFEGrainType(beamKey: FEGrainType){
  const backendGrainType = findBEGrainTypeForFEGrainType(beamKey)
  return MAP_BE_GRAIN_TYPE_TO_PREVIEW_DATA_SERIES_TYPE[backendGrainType]
}


const MAP_HC_SERIES_TYPE_TO_HC_DATA_FORMAT: {
  [K in HcSeriesType]: HcDataFormat
} = {
  [HcSeriesType.LINE]: HcDataFormat.XY_ARRAY,
  [HcSeriesType.CANDLESTICK]: HcDataFormat.XY_ARRAY,
  [HcSeriesType.COLUMN]: HcDataFormat.XY_ARRAY,
  [HcSeriesType.SCATTER]: HcDataFormat.XY_OBJECT,
  [HcSeriesType.AREARANGE]: HcDataFormat.HIGH_LOW_OBJECT,
}

const MAP_BE_GRAIN_TYPE_TO_PREVIEW_DATA_SERIES_TYPE: {
  [K in BEGrainType]: HcSeriesType
} = {
  [BEGrainType.MIN]: HcSeriesType.CANDLESTICK,
  [BEGrainType.SEC]: HcSeriesType.CANDLESTICK,
  [BEGrainType.FULL]: HcSeriesType.SCATTER,
}

