import { TableDataType } from '../components/TableInteractive';
import { Colors } from '../constants';
import { PayloadCsvSummary } from '../models';
import { BEVenueTypes } from '../types';
import { rgbToRgba } from '../util';


export function findLabelForCsvFillKey(fillsKey: keyof PayloadCsvFillWithTs){
  return MAP_CSV_FILL_KEY_TO_LABEL[fillsKey]
}

export function findDataTypeForCsvFillKey(fillsKey: keyof PayloadCsvFillWithTs){
  return MAP_CSV_FILL_KEY_TO_DATA_TYPE[fillsKey]
}

export function findColumnWForCsvFillLabel(label: string){
  return MAP_CSV_FILL_LABEL_TO_TABLE_COLUMN_W[label]
}

export function findLabelForCsvSummaryKey(summaryKey: keyof PayloadCsvSummary){
  return MAP_CSV_SUMMARY_KEY_TO_LABEL[summaryKey]
}

export function findColorForVenue(venueKey: BEVenueTypes){
  return MAP_VENUE_CODES_TO_COLOR[venueKey]
}

export function findLabelForVenue(venueKey: BEVenueTypes){
  return MAP_VENUE_CODES_TO_LABEL[venueKey]
}


export const ADD_ON_TS_KEY = 'Timestamp'

// --------------------------
// Uploaded CSV Fills Trades Metrics
// --------------------------
export type PayloadCsvFill = {
  TIME: number // 1560438025000.0
  SYMBOL: string// "UBNT"
  SIZE: number // 23
  PRICE: number // 132.59
  VENUE: BEVenueTypes // "EDGX"
  SIDE: string // "Buy"
  matched: number // 0, 1 , 2...
  markout: number // 0
  markout_px: number // 1325900
  markout_time: number // 1560438025683.7817
}

export type PayloadCsvFillWithTs = PayloadCsvFill & {
  [ADD_ON_TS_KEY]: number
}

export const MAP_CSV_FILL_KEY_TO_LABEL: {
  [k in keyof PayloadCsvFillWithTs]: string
} = {
  TIME: 'Time',
  SYMBOL: 'Symbol',
  SIZE: 'Size',
  PRICE: 'Price',
  VENUE: 'Venue',
  SIDE: 'Side',
  matched: 'Matched',
  markout: 'Markout (1s)',
  markout_px: 'Markout Price (1s)',
  markout_time: 'Markout Time (1s)',
  [ADD_ON_TS_KEY]: ADD_ON_TS_KEY,
}


const MAP_CSV_FILL_KEY_TO_DATA_TYPE: {
  [k in keyof PayloadCsvFillWithTs]: TableDataType
} = {
  TIME: TableDataType.TEXT,
  SYMBOL: TableDataType.TEXT,
  SIZE: TableDataType.NUMBER,
  PRICE: TableDataType.NUMBER,
  VENUE: TableDataType.TEXT,
  SIDE: TableDataType.TEXT,
  matched: TableDataType.NUMBER,
  markout: TableDataType.NUMBER,
  markout_px: TableDataType.NUMBER,
  markout_time: TableDataType.TEXT,
  [ADD_ON_TS_KEY]: TableDataType.NUMBER,
}

const MAP_CSV_FILL_KEY_TO_TABLE_COLUMN_W: {
  [k in keyof PayloadCsvFillWithTs]: number
} = {
  TIME: 190,
  SYMBOL:90,
  SIZE: 80,
  PRICE: 80,
  VENUE: 80,
  SIDE: 80,
  matched: 80,
  markout: 110,
  markout_px: 150,
  markout_time: 150,
  [ADD_ON_TS_KEY]: 150,
}

const MAP_CSV_FILL_LABEL_TO_TABLE_COLUMN_W: Record<string, number> = (function(){
  const keys = Object.keys(MAP_CSV_FILL_KEY_TO_LABEL) as (keyof PayloadCsvFillWithTs)[]
  return keys.reduce((prev: Record<string, number>, key)=>{
    const label = MAP_CSV_FILL_KEY_TO_LABEL[key]
    const columnW = MAP_CSV_FILL_KEY_TO_TABLE_COLUMN_W[key]
    prev[label] = columnW
    return prev
  }, {})
}())


export const MAP_CSV_SUMMARY_KEY_TO_LABEL: {
  [k in keyof PayloadCsvSummary]: string
} = {
  volume: 'Volume',
  count: 'Fills Count',
  avg_size: 'Avg. Size',
  avg_price: 'Avg. Price',
  arrival: 'Arrival Price',
  vwap: 'Interval VWAP',
  end_midpoint: 'End Midpoint',
  reversion_px: 'Reversion Price',
  slippage_arrival: 'Arrival Slippage',
  slippage_vwap: 'VWAP Slippage',
  reversion: 'Reversion (3min)',
  avg_markout: 'Avg. Markout',
}

const MAP_VENUE_CODES_TO_LABEL : {
  [k in BEVenueTypes]: string
} = {
  // NYSE
  XNYS: 'New York Stock Exchange',
  ARCX: 'NYSE Arca',
  XASE: 'NYSE American (AMEX)',
  XCIS: 'National Stock Exchange',
  // Nasdaq
  XNGS: 'Nasdaq',
  XBOS: 'NASDAQ OMX BX',
  XPHL: 'Nasdaq PSX',
  // Cboe
  EDGA: 'Cboe EDGA',
  EDGX: 'Cboe EDGX',
  BATY: 'Cboe BYX',
  BATS: 'Cboe BZX',
  // Misc
  IEXG: 'IEX',
  XCHI: 'Chicago Stock Exchange, Inc',
  FINR: 'FINRA',
  [BEVenueTypes.UNKNOWN]: BEVenueTypes.UNKNOWN,
}


const MAP_VENUE_CODES_TO_COLOR : {
  [k in BEVenueTypes]: string
} = {
  // NYSE
  XASE: Colors.VENUE_NYSE_FAM,
  ARCX: rgbToRgba(Colors.VENUE_NYSE_FAM, 0.8),
  XCIS: rgbToRgba(Colors.VENUE_NYSE_FAM, 0.6),
  XNYS: rgbToRgba(Colors.VENUE_NYSE_FAM, 0.4),
  // Nasdaq
  XNGS: Colors.VENUE_NASDAQ_FAM,
  XBOS: rgbToRgba(Colors.VENUE_NASDAQ_FAM, 0.8),
  XPHL: rgbToRgba(Colors.VENUE_NASDAQ_FAM, 0.6),
  // Cboe
  EDGA: Colors.VENUE_CBOE_FAM,
  EDGX: rgbToRgba(Colors.VENUE_CBOE_FAM, 0.8),
  BATY: rgbToRgba(Colors.VENUE_CBOE_FAM, 0.6),
  BATS: rgbToRgba(Colors.VENUE_CBOE_FAM, 0.4),
  // Misc
  IEXG: Colors.VENUE_IEX,
  XCHI: Colors.VENUE_CHX,
  FINR: Colors.VENUE_FINRA,
  [BEVenueTypes.UNKNOWN]: Colors.VENUE_UNKNOWN,
}