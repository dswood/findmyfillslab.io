import { ButtonGroup } from '@blueprintjs/core';
import React from 'react';
import RangeDisplay from '../components/RangeDisplay';
import RangeSelector from '../components/RangeSelector';

type Props = {

}

class TimeControls extends React.Component<Props> {

  render(){
    return (
      <div id="container-time-controls">
        <style jsx>{`
          #container-time-controls {
            display: flex;
            flex-wrap: nowrap;
            overflow: auto;
            white-space: nowrap;
          }
        `}</style>
        <ButtonGroup>
          <RangeSelector
            />
          <RangeDisplay
            />
        </ButtonGroup>
      </div>
    )
  }
}

export default TimeControls