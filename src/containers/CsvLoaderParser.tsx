import { Button, Dialog, H4, InputGroup, IToastProps } from '@blueprintjs/core';
import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AddToastAction, createFetchSymbolDataAction, createGetCsvFillsMetricsAction, createSelectSymbolAction, createSetDayStartAction, createSetSelectedFmfCsvData, createSetTopVisibleBeamAction, FetchSymbolDataAction, GetCsvFillsMetricsAction, SelectSymbolAction, SetDayStartAction, SetSelectedFmfCsvData, SetTopVisibleBeamAction } from '../actions';
import FileTable, { CsvParams } from '../components/CsvFileTable';
import HeaderMapper, { createHeaderMap, getCsvConfigStatus, IHeaderMapping, initMappingsFromHeaderKeys, normalizeCsv, updateHeaderMapping } from '../components/CsvHeaderMapper';
import { checkIfConfigsAreValid, surveyForCsvParams } from '../components/CsvHeaderMapper/util';
import CsvHighLvlParamsSelect from '../components/CsvHighLvlParamsSelect';
import CsvImporter from '../components/CsvImporter';
import CsvPreviewTable from '../components/CsvPreviewTable';
import { createReqToast, createUploadCsvToast, withToasts } from '../components/Toaster';
import WarningCallout from '../components/WarningCallout';
import { DUR_TRADING_DAY } from '../constants';
import { SAMPLE_CSV_UBNT_190613 } from '../models';
import { findBEGrainTypeForFEGrainType } from '../records';
import { createCsvId, DEV_ONLY_SET_CSV_ENDPOINT, IApplicationState, selectBeamsByKeys } from '../reducers';
import { INavBeam } from '../schemas/navigator';
import { CsvConfigStatusTypes, CsvSideType, CsvTimezoneType, FEGrainType } from '../types';
import { getLast, getTradingDayStart, updateArrayItem } from '../util';
import { AuthUserContext } from './FirebaseLogin';


type Props = {
  enableDevOnlyUrlInput: boolean
} & StoreProps & DispatchProps & IHocProps

type IHocProps = {
  addToast: AddToastAction
}

type State = {
  isHeaderMapperActive: boolean
  editFileIndex: number
  csvParamInfos: CsvParams[]
}

const DIALOG_INITIAL_STATES = {
  isHeaderMapperActive: false,
  editFileIndex: 0,
}

const overallStatuses = [
  // CsvConfigStatusTypes.INCORRECT_SYMBOL_HEADER_MAPPING,
  // CsvConfigStatusTypes.MULTIPLE_SYMBOLS_IN_CSV,
  // CsvConfigStatusTypes.HAS_UNDEFINED_HEADER_MAPPINGS,
  // CsvConfigStatusTypes.INCOMPLETE_HEADER_MAPPINGS,
  // CsvConfigStatusTypes.INVALID_DATE_SELECTION,
  CsvConfigStatusTypes.ALL_CONFIGS_VALID,
]
const headerMappingStatuses = [
  CsvConfigStatusTypes.INCORRECT_SYMBOL_HEADER_MAPPING,
  // CsvConfigStatusTypes.MULTIPLE_SYMBOLS_IN_CSV,
  CsvConfigStatusTypes.HAS_UNDEFINED_HEADER_MAPPINGS,
  CsvConfigStatusTypes.INCOMPLETE_HEADER_MAPPINGS,
  // CsvConfigStatusTypes.INVALID_DATE_SELECTION,
  // CsvConfigStatusTypes.ALL_CONFIGS_VALID,
]
const highLvlConfigStatuses = [
  // CsvConfigStatusTypes.INCORRECT_SYMBOL_HEADER_MAPPING,
  CsvConfigStatusTypes.MULTIPLE_SYMBOLS_IN_CSV,
  // CsvConfigStatusTypes.HAS_UNDEFINED_HEADER_MAPPINGS,
  // CsvConfigStatusTypes.INCOMPLETE_HEADER_MAPPINGS,
  CsvConfigStatusTypes.INVALID_DATE_SELECTION,
  // CsvConfigStatusTypes.ALL_CONFIGS_VALID,
]




class CsvLoaderParser extends React.Component<Props, State> {

  static contextType = AuthUserContext

  constructor(props: Props){
    super(props)
    this.state = Object.assign(
      DIALOG_INITIAL_STATES, {
        csvParamInfos: [
          // SAMPLE_CSV_BAC_FAKE_190613,
          SAMPLE_CSV_UBNT_190613,
        ],
      }
    )
  }

  handleImportedCsv = (csvHeader: string[], csvBody: string[][], fileName: string) => {
    const fullCsv = [csvHeader].concat(csvBody)

    const originalHeaderKeys = fullCsv[0].slice()
    const headerMappings = initMappingsFromHeaderKeys(originalHeaderKeys)

    const configStatus: Partial<CsvParams> = surveyForCsvParams(csvBody, headerMappings)
    /*
     https://momentjs.com/docs/#/parsing/string/
     When creating a moment from a string, we first check if the string matches known ISO 8601 formats, we then check if the string matches the RFC 2822 Date time format before dropping to the fall back of new Date(string) if a known format is not found.
    */
    const mDate = moment(
      configStatus.sampleDateValueInCsv !== undefined ?
        configStatus.sampleDateValueInCsv
          : 'A string value that parses to a invalid moment object'
    ) // try to parse date value in csv

    // Prepare object for final validation below...
    const uploadTime = Date.now()
    const newCsvParamInfo: CsvParams = {
      uploadTime,
      fileId: createCsvId(fileName, uploadTime),
      fileName,
      originalCsv: fullCsv,
      originalHeaderKeys,
      normalizedCsv: undefined,
      headerMappings,
      targetTimezone: CsvTimezoneType.UTC, // default to UTC
      targetDate: mDate.isValid() ? mDate.format('YYYY-MM-DD') : 'YYYY-MM-DD',
      isCsvConfigValid: false,
      ...configStatus
    }
    // Check if all configurations are valid
    Object.assign(newCsvParamInfo, {
      isCsvConfigValid: checkIfConfigsAreValid(getCsvConfigStatus(newCsvParamInfo))
    })
    // console.log('new surveryed csv params', newCsvParamInfo)
    this.setState({
      csvParamInfos: this.state.csvParamInfos.concat([ newCsvParamInfo ]),
    }, ()=>{
      const { csvParamInfos } = this.state
      this.handleEditFileInfo(csvParamInfos.length - 1) // put newly uploaded file up for edit
    })
  }

  handleHeaderMappingChange = (editFileIndex: number, updatedMapping: IHeaderMapping)=>{
    const { csvParamInfos } = this.state
    const csvParamsToEdit = csvParamInfos[editFileIndex]; if(csvParamsToEdit === undefined) return;

    const csvBody = csvParamsToEdit.originalCsv.slice(1)
    const updatedHeaderMappings = updateHeaderMapping(csvParamsToEdit.headerMappings, updatedMapping)

    const configStatus: Partial<CsvParams> = surveyForCsvParams(csvBody, updatedHeaderMappings)
    // console.log('configStatus',configStatus)
    const mDate = moment(
      configStatus.sampleDateValueInCsv !== undefined ?
        configStatus.sampleDateValueInCsv
          : 'A string value that parses to a invalid moment object'
    ) // try to parse date value in csv

    // Prepare object for final validation below...
    const updatedCsvParamInfo: CsvParams = Object.assign({}, csvParamsToEdit, {
      headerMappings: updatedHeaderMappings,
      targetDate: mDate.isValid() ? mDate.format('YYYY-MM-DD') : csvParamsToEdit.targetDate, // Overide if new parseable date discovered, else keep user selected date. For edge case where timestamp header was not initially mapped, but corrected later
      ...configStatus
    })

    // Check if all configurations are valid
    Object.assign(updatedCsvParamInfo, {
      isCsvConfigValid: checkIfConfigsAreValid(getCsvConfigStatus(updatedCsvParamInfo))
    })

    this.setState({
      csvParamInfos: updateArrayItem(csvParamInfos.slice(), editFileIndex, updatedCsvParamInfo)
    })
  }

  handleCsvHighLvlParamChange<T>(editFileIndex: number, paramKey: string, paramVal: T){
    const { csvParamInfos } = this.state
    const csvParamsToEdit = csvParamInfos[editFileIndex]
    if (csvParamsToEdit !== undefined){
      // console.log('handleCsvHighLvlParamChange --->', paramKey, paramVal)
      const updatedCsvParamInfo: CsvParams = Object.assign({}, csvParamsToEdit, { [paramKey]: paramVal })

      // Check if all configurations are valid
      Object.assign(updatedCsvParamInfo, {
        isCsvConfigValid: checkIfConfigsAreValid(getCsvConfigStatus(updatedCsvParamInfo))
      })

      this.setState({
        csvParamInfos: updateArrayItem(csvParamInfos.slice(), editFileIndex, updatedCsvParamInfo)
      })
    }
  }

  uploadNormalizedCsv = (csvParams: CsvParams): void => {
    const { devOnlyCsvEndpoint, enableDevOnlyUrlInput } = this.props
    const { context: authUser } = this
    if(
      csvParams.normalizedCsv !== undefined
      && csvParams.targetSymbol !== undefined
    ){
      console.log('uploadNormalizedCsv authUser context --->', authUser)
      // console.log('uploadNormalizedCsv', csv)
      this.props.getCsvFillsMetrics(
        csvParams.fileId,
        csvParams.normalizedCsv,
        csvParams.targetSymbol,
        csvParams.targetDate, // targetDate,
        csvParams.targetTimezone,
        csvParams.targetSide,
        enableDevOnlyUrlInput ? devOnlyCsvEndpoint : undefined
      )
      if(!process.env.IS_PROD){
        this.props.addToast(createUploadCsvToast(csvParams))
      }
    }
  }

  handleRemoveFile = (fileIndex: number) => {
    const { csvParamInfos } = this.state
    this.setState({
      csvParamInfos: csvParamInfos.filter((info, i) => i !== fileIndex)
    })
  }

  handleEditFileInfo = (fileIndex: number) => {
    this.setState({
      isHeaderMapperActive: true,
      editFileIndex: fileIndex
    })
  }

  resetToLowestTimeGrain(){
    const { activeTimeGrainKeys, possibleTimeGrains } = this.props
    const activeTimeGrains = selectBeamsByKeys(possibleTimeGrains, activeTimeGrainKeys)
    const lowestGrain = getLast(activeTimeGrains)
    this.props.selectTimeGranularity(
      possibleTimeGrains,
      activeTimeGrainKeys,
      lowestGrain.key, {
        riderStart: 0,
        riderSize: lowestGrain.beamSize,
      }
    )
  }

  prepAppForUserFills(targetDate: string, targetSymbol: string){
    // table setting for new FMF request, for possible new symbols and date
    const { selectedSymbolKey, selectedDayStartUtc } = this.props
    const currentDate = moment(selectedDayStartUtc)

    const didDateChange = targetDate !== currentDate.format('YYYY-MM-DD')
    const didSymbolChange = targetSymbol !== selectedSymbolKey
    // -------------------------
    // fetch new symbol data if csv specifies a different symbol key than what is current displayed...
    if(didDateChange && didSymbolChange){ // BOTH symbol and date changed...
      const backendGrainType = findBEGrainTypeForFEGrainType(FEGrainType.DAY)
      const [ yyyy, mm, dd ] = targetDate.split('-')
      const tradingDayStartUtc = getTradingDayStart(yyyy, mm, dd)
      const tradingDayEndUtc = tradingDayStartUtc + DUR_TRADING_DAY
      if(!process.env.IS_PROD){
        this.props.addToast(createReqToast(
          backendGrainType,
          targetSymbol,
          tradingDayStartUtc,
          tradingDayEndUtc
        ))
      }
      this.props.fetchSymbolData(
        backendGrainType,
        targetSymbol,
        tradingDayStartUtc,
        tradingDayEndUtc
      )
    }

    if(didDateChange){
      this.props.setSelectedDayStart(moment(targetDate).toDate())
    } else {
      this.resetToLowestTimeGrain() // NOTE: On date change, date value diff is detected in RangeSelector Component and grain type is automatically reset to lowest active beam. However in the edge case where the current date value in the StockChart is already the same as the FMF event, the granularity reset will not be triggered by RangeSelector (since no diff will be dtected), and so manually trigger reset here.
    }
    if(didSymbolChange && targetSymbol !== undefined){
      this.props.setSelectedSymbol(targetSymbol)
    }
  }

  handleUploadCsvToBackend = (fileIndex: number) => {
    const { csvParamInfos } = this.state
    const fileInfo = csvParamInfos[fileIndex]
    const { targetDate, targetSymbol, headerMappings } = fileInfo
    // NOTE: do not get smart and try to not make data calls for csv already previously fetched, or reuse normalized csv.  This shouldn't be done because user may go back and change csv indepedent (optionally) like Timezone or Side, all of which changes the resultant normalized csv. To try to be efficient and manage these changes is not worth the effort, thus regenerating normalized csv on every FMF call will be most straight forward and fool proof way to make sure user's most recent configurations is reflected.
    this.prepAppForUserFills(targetDate, targetSymbol)
    const mapToNormalizedHeader = createHeaderMap(headerMappings)
    const normalizedCsv = normalizeCsv(fileInfo, mapToNormalizedHeader)
    this.setState({
      csvParamInfos: updateArrayItem(csvParamInfos.slice(), fileIndex, {
        normalizedCsv,
      })
    }, ()=>{ // normalized csv is created on first run...
      const { csvParamInfos } = this.state
      this.uploadNormalizedCsv(csvParamInfos[fileIndex])
    })
  }

  closeHeaderMapper = () => {
    this.setState({ isHeaderMapperActive: false })
  }

  renderDevOnlyUrlInput(){
    // TODO: for dev testing, remove for rollout
    const { devOnlyCsvEndpoint, enableDevOnlyUrlInput } = this.props
    return enableDevOnlyUrlInput ? (
      <InputGroup
        large={true}
        leftIcon="log-out"
        onChange={(e: React.FormEventHandler<HTMLElement>)=>{
          this.props.devOnlySetCsvEndpoint(e.target.value)
        }}
        placeholder="url here..."
        value={devOnlyCsvEndpoint}
        />
    ) : null
  }

  renderCsvConfigurationDialog(csvParamToEdit: CsvParams){
    const { isHeaderMapperActive, editFileIndex } = this.state
    const { fileName, uploadTime, targetSymbol, targetTimezone, targetSide, targetDate, uniqueSymbolsInCsv, originalHeaderKeys, originalCsv, sampleDateValueInCsv } = csvParamToEdit

    const configStatus = getCsvConfigStatus(csvParamToEdit)

    return (
      <Dialog
        onClose={this.closeHeaderMapper}
        isOpen={isHeaderMapperActive}
        title={`Configure Headers: ${fileName}`}
        >
        <div className="dialog-content-container">
          <WarningCallout
            className="h-mapper-warning-callout"
            status={overallStatuses.find(s => s === configStatus)}
            csvInfo={csvParamToEdit}
            />

          <div className="csv-preview-container">
            <p>{`Imported at: [${new Date(uploadTime).toUTCString()}]`}</p>
            <CsvPreviewTable
              headerRow={originalHeaderKeys}
              bodyRows={originalCsv.slice(1, 6)}
              columnW={100}
              />
          </div>

          <div className="csv-highlvl-config-container">
            <H4>High-level Configurations</H4>
            <p><b>IMPORTANT</b>: Please make sure to select a <b>timezone</b> that correctly reflects the time values found in your csv!</p>
            <WarningCallout
              className="h-mapper-warning-callout"
              status={highLvlConfigStatuses.find(s => s === configStatus)}
              csvInfo={csvParamToEdit}
              />
            <CsvHighLvlParamsSelect
              targetSymbol={targetSymbol}
              targetTimezone={targetTimezone}
              targetSide={targetSide}
              targetDate={targetDate}
              uniqueSymbolsInCsv={uniqueSymbolsInCsv}
              sampleDateValueInCsv={sampleDateValueInCsv}
              onTargetSymbolChange={(symbol: string) => {
                this.handleCsvHighLvlParamChange(editFileIndex, 'targetSymbol', symbol)
              }}
              onTargetTimezoneChange={(timezone: CsvTimezoneType) => {
                this.handleCsvHighLvlParamChange(editFileIndex, 'targetTimezone', timezone)
              }}
              onTargetSideChange={(side: CsvSideType) => {
                this.handleCsvHighLvlParamChange(editFileIndex, 'targetSide', side)
              }}
              onTargetDateChange={(mmddyyyy: string) => {
                this.handleCsvHighLvlParamChange(editFileIndex, 'targetDate', mmddyyyy)
              }}
              />
          </div>

          <H4>Header Mappings</H4>
          <WarningCallout
            className="h-mapper-warning-callout"
            status={headerMappingStatuses.find(s => s === configStatus)}
            csvInfo={csvParamToEdit}
            />
          <HeaderMapper
            csvInfo={csvParamToEdit}
            onHeaderMappingChange={(updatedMapping: IHeaderMapping)=>{
              this.handleHeaderMappingChange(editFileIndex, updatedMapping)
            }}
            />

          <Button
            icon="tick"
            intent="primary"
            text="Save Mapping"
            minimal={true}
            onClick={this.closeHeaderMapper}
            />
        </div>
      </Dialog>
    )
  }

  render() {
    const { csvParamInfos, editFileIndex } = this.state
    const fileInfoToEdit = csvParamInfos[editFileIndex]

    return (
      <div id="container-csv-importer-and-file-table">
        {this.renderDevOnlyUrlInput()}
        <CsvImporter
          onCsvImported={this.handleImportedCsv}
          />
        <FileTable
          shouldRenderRowsReversed={true}
          csvInfos={csvParamInfos}
          onFileRemove={this.handleRemoveFile}
          onFileEdit={this.handleEditFileInfo}
          onUploadCsvToBackend={this.handleUploadCsvToBackend}
          />
        {
          fileInfoToEdit !== undefined ?
            this.renderCsvConfigurationDialog(fileInfoToEdit)
              : null
        }
      </div>
    )
  }
}

type StoreProps = {
  avialableUserFillsSlices: string[]
  toasts: IToastProps[]
  selectedSymbolKey: string
  selectedDayStartUtc: number
  possibleTimeGrains: INavBeam[]
  activeTimeGrainKeys: (FEGrainType)[]
  devOnlyCsvEndpoint: string
}

const mapStateToProps = ({ appControls, navigator, userFills: fillsMetrics }: IApplicationState): StoreProps => ({
  avialableUserFillsSlices: Object.keys(fillsMetrics),
  toasts: appControls.toasts,
  selectedSymbolKey: appControls.selectedSymbolKey,
  selectedDayStartUtc: navigator.selectedDayStartUtc,
  possibleTimeGrains: navigator.navBeams,
  activeTimeGrainKeys: navigator.activeBeamKeys,
  devOnlyCsvEndpoint: appControls.devOnlyCsvEndpoint,
})


type DispatchProps = {
  getCsvFillsMetrics: GetCsvFillsMetricsAction
  setSelectCsvFillsMetrics: SetSelectedFmfCsvData
  setSelectedSymbol: SelectSymbolAction
  setSelectedDayStart: SetDayStartAction
  selectTimeGranularity: SetTopVisibleBeamAction
  fetchSymbolData: FetchSymbolDataAction
  devOnlySetCsvEndpoint: (url: string) => void
}
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  setSelectedSymbol: createSelectSymbolAction(dispatch),
  getCsvFillsMetrics: createGetCsvFillsMetricsAction(dispatch),
  setSelectCsvFillsMetrics: createSetSelectedFmfCsvData(dispatch),
  setSelectedDayStart: createSetDayStartAction(dispatch),
  selectTimeGranularity: createSetTopVisibleBeamAction(dispatch),
  fetchSymbolData: createFetchSymbolDataAction(dispatch),
  devOnlySetCsvEndpoint: (url)=> dispatch({
    type: DEV_ONLY_SET_CSV_ENDPOINT,
    data: url
  })
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withToasts(CsvLoaderParser));

