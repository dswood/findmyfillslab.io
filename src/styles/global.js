import css from 'styled-jsx/css';
import { Colors } from '../constants';
import { rgbToRgba } from '../util';

export default css.global`

* {
  box-sizing: border-box;
  font-family: 'Roboto', sans-serif;
  /* font-size: 16px; */
}

.legal-doc-container {
  max-width: 740px;
  margin: 0 auto;
  padding: 25px;
}
.doc-title {
  margin-bottom: 50px;
}

#app-title{
  font-weight: 100;
  letter-spacing: -1px;
  text-align: center;
  margin: 0;
}

body {
  line-height: 1.1;
  color: ${Colors.ACCENT_1};
  background: ${Colors.CANVAS};
  /* padding: 50px 100px; */
  /* margin: 0 auto; */
  /* max-width: 960px; */
}

svg {
  user-select: none;
}

.overflow {
  overflow: auto;
}



.panel-header h1 {
  margin: 0px 20px 0px 0px;
  letter-spacing: 1px;
  font-size: 59px;
  color: ${Colors.ACCENT_1};
  /* color: ${Colors.ACCENT_1}; */
}
.panel-left-right-container {
  width: 100%;
  display: flex;
}

.panel-left > div:last-child {
  /* overflow: auto; */
  height: 100%;
}


#symbol-search-container {
  line-height: 36px;
  flex-grow: 1;
}
#symbol-search-container .Select-menu-outer {
  z-index: 999999;
}



#container-stock-chart {
  flex-grow: 1;
  height: 0; /*
  As long as the flex container has no height itself, you can set height: 0 on the first flex item. Because it has no height to inherit from its parent, any percentage height will cause it to collapse. It will then grow with its contents.
  */
}

#container-highstock {
  height: 100%;
}

#multi-beam-navigator-container .nav-beam-container:not(:last-child) {
  margin-bottom: 5px
}


.app-section {
  width: 100%;
  margin-bottom: 15px;
}

.flex {
  display: flex;
}


.with-border {
  border: 1px solid rgba(16, 22, 26, 0.15);
}


.beam-step-arrow {
  margin-top: 20px;
}





.tag {
  font-size: 12px;
  padding: 1px 7px;
  border-radius: 3px;
  margin: 0;
  cursor: pointer;
  user-select: none;
}

.beam-tag {
  background: ${Colors.ACCENT_1};
  color: #fff;
  cursor: auto;
}
.widget-tag {
  color: ${Colors.BP3_DISABLED_BTN_TEXT};
  background: ${Colors.BP3_DISABLED_BTN};
}

.tag-disabled {
  color: #ccc;
  cursor: not-allowed;
}


.widget-tag:hover {
  background: rgb(16, 107, 163);
  color: #fff;
}



#h-mapper-container {
}
.h-mapper-row-divider {
  width: 100%;
  border-bottom: 1px solid rgba(16, 22, 26, 0.15);
  margin-bottom: 12px;
}
.h-mapper-row-container:first-child {
  font-weight: 600;
}
.h-mapper-row-container {
  display: flex;
  justify-content: flex-start;
  align-items: baseline;
}

.h-mapper-row-field:nth-child(1n) {
  flex: 0 0 6%;
}
.h-mapper-row-field:nth-child(2n) {
  flex: 0 1 auto; /* auto retains original width */
}
.h-mapper-row-field:nth-child(3n) {
  flex: 0 1 25%;
  margin-left: auto; /* right align item inside flex row*/
}


body .h-mapper-warning-callout span {
  font-size: 13px;
  font-weight: 200;
  font-style: italic;
}


.dialog-content-container {
  padding: 25px 25px 7px 25px;
}

.dialog-content-container > div {
  margin-bottom: 25px;
}

/* body .csv-reader-table-row td {
  vertical-align: middle;
} */


.text-disabled {
  text-decoration: line-through;
  color: #aaa;
}

.text-disclaimer {
  display: inline-block;
  font-size: 10px;
  font-weight: 100;
  letter-spacing: 1.0px;
  margin: 0px;
  color: ${Colors.ACCENT_1};
}

.text-small {
  font-size: 12px;
  font-weight: 200;
  margin: 0px;
  color: ${Colors.ACCENT_1};
}

.text-right {
  text-align: right;
}
.text-center {
  text-align: center;
}

.slide-rule-container text {
  font-size: 9px;
}

.small-tag {
  display: inline-block;
  background-color: #aaa;
  color: white;
  font-size: 9px;
  font-weight: 400;
  line-height: 10px;
  padding: 1.5px 4px;
  border-radius: 2px;
  letter-spacing: 0.5px;
}

.blur {
  -webkit-filter: blur(3px);
  -moz-filter: blur(3px);
  -ms-filter: blur(3px);
  -o-filter: blur(3px);
  filter: blur(3px);
}

.full-height {
  height: 100%;
}

.spinner-clock-container .separator {
  padding: 0 2px;
}
.spinner-clock-digits .bp3-tag {
  padding: 0 5px;
  font-size: 16px;
}
.spinner-clock-digits--flash {
  animation: spinner-clock-digits--flash 0.2s ease-in-out 1;
}

@keyframes spinner-clock-digits--flash {
  50% {
    opacity: 0.25;
  }
}

.relative {
  position: relative;
}




/* ------------------
  Blueprintjs Overrides
  ------------------- */
.bp3-dialog{
  background-color: ${Colors.CANVAS};
}
.bp3-input, .bp3-heading{
  color: ${Colors.ACCENT_1};
}
table.bp3-html-table td {
  color: ${Colors.ACCENT_1};
}
.bp3-button:not([class*="bp3-intent-"]){
  color: ${Colors.ACCENT_1};
}
.bp3-datepicker-footer button:last-child {
  /* disable clear button on date picker */
  display: none;
}
.bp3-button.bp3-minimal.bp3-intent-danger {
  color: ${Colors.ACCENT_2};
}
.bp3-button.bp3-intent-primary, .bp3-tag.bp3-intent-primary  {
  background-color: ${Colors.ACCENT_1};
}
.bp3-button.bp3-intent-primary:hover {
  background-color: ${Colors.ACCENT_2};
}
.bp3-button .bp3-icon {
  color: ${Colors.ACCENT_1};
}
.bp3-button.bp3-minimal.bp3-intent-primary{
  color: ${Colors.ACCENT_1};
}
/* button primary intent minimal */
.bp3-button.bp3-intent-primary.bp3-minimal {
  background-color: ${Colors.TRANSPARENT};
}
/* button primary intent disabled */
.bp3-button.bp3-intent-primary:disabled, .bp3-button.bp3-intent-primary.bp3-disabled {
  background-color: rgba(92, 112, 128, 0.6);
  /* background-color: ${rgbToRgba(Colors.ACCENT_2, 0.5)}; */
}

.bp3-control input:disabled:checked ~ .bp3-control-indicator {
  background-color: rgba(92, 112, 128, 0.6);
}

.bp3-control input:checked ~ .bp3-control-indicator {
  background-color: ${Colors.ACCENT_1};
}
.bp3-table-bottom-container, .bp3-table-column-headers {
  color: ${Colors.ACCENT_1};
}
.bp3-button.bp3-intent-primary:disabled, .bp3-button.bp3-intent-primary.bp3-disabled {
}


/* ------------------
  React-Select Overrides
  ------------------- */
.Select.has-value.Select--single > .Select-control .Select-value .Select-value-label {
  color: ${Colors.ACCENT_1};
}


.date-picker-container {
  width: 100px;
}
.date-picker-container *{
  height: 100%;
}

.time-picker-container {
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}

.time-picker-container.invalid-time .bp3-timepicker-input-row {
  border: 1px solid ${Colors.ACCENT_2};
}


.highcharts-background {
  cursor: ew-resize
}

.grain-selector-container {
  padding: 25px;
}

.timezone-picker-button {
  height: 100%;
}


.h-mapper-row-field.Select-placeholder, .h-mapper-row-field.Select--single > .Select-control .Select-value {
  line-height: 20px;
}

.h-mapper-row-field .Select-control {
  height: 20px;
}
.h-mapper-row-field .Select-input {
  height: 20px;
}



.thin-scrollbar::-webkit-scrollbar-track {
	background-color: #eee;
}
.thin-scrollbar::-webkit-scrollbar {
	width: 3px;
	background-color: #eee;
}
.thin-scrollbar::-webkit-scrollbar-thumb {
	background-color: rgb(200,200,200);
}


.component-with-tooltip .bp3-popover-target {
	width: 100%;
	display: block;
}


`

/* background-color: #5c7080; */

/* background - color: rgba(92, 112, 128, 1.0)
rgba(138, 155, 168, 0.2) */