# Contributing to FindMyFills
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

## We Develop with Gitlab
We use gitlab to host code, to track issues and feature requests, as well as accept pull requests.

## Making Contributions
Pull requests are the best way to propose changes to the codebase. We actively welcome your pull requests:

1. Fork the repo and create your branch from `master`.
5. Make sure your code lints.
6. Issue that pull request!

If you are unable to create Pull requests or test your changes, but believe that you have a valuable change, feel free to mail us a patch file at fmf@prooftrading.com. 

## Report bugs using GitLab [issues](https://gitlab.com/findmyfills/findmyfills-api/issues)
- We use GitLab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/findmyfills/findmyfills.gitlab.io/issues);
- Write bug reports with detail, background, and sample code

## Use a Consistent Coding Style

* For the most part, adopt the coding style of the file you are editing
* We follow [Airbnb's JavaScript Style Guide](https://github.com/airbnb/javascript) and [Microsoft's TypeScript Style Guide](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines).

## License
By contributing, you agree that your contributions will be licensed under its MIT License.

