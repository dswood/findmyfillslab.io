![Build Status](https://gitlab.com/findmyfills/findmyfills.gitlab.io/badges/master/build.svg)

# Find My Fills Front End

Find My Fills is a single stock exploration tool that allow users to drill down into ticks for a given symbol and match their executions to trades on the tape.  Developed and maintained by [Proof](https://prooftrading.com).  

This repo contains the front end for findmyfills.com, built with Next.js and served using GitLab Pages.  For more information check out our [introductory blog post](https://medium.com/prooftrading/find-my-fills-introducing-a-new-tool-for-visualizing-market-activity-at-every-timescale-4bb7df2dee0e) and [technical deep dive](https://medium.com/prooftrading) on Medium, or give us a shout at dev@prooftrading.com.


## License

This project is made available under MIT License.
